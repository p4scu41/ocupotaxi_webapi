<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    // Formato de la respuesta json
    public static $json_format = [
        // Datos extras que se requieran
        'extra'   => null,
        // Datos solicitados
        'data'    => [],
        // Mensaje breve descriptivo de la respuesta
        'message' => null,
        // Código de la respuesta HTTP
        'status'  => null,
        // Código del error
        'error' => null
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('jsonFormat', function ($data = [], $status = 200, $headers = []) {
            $content = array_merge(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            return response()->json($content, $status, $headers);
        });

        Response::macro('jsonSuccess', function ($data = [], $headers = []) {
            $status = 200;
            $content = array_merge(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            if (empty($content['message'])) {
                $content['message'] = 'Solicitud procesada exitosamente';
            }

            return response()->json($content, $status, $headers);
        });

        Response::macro('jsonNotFound', function ($data = [], $headers = []) {
            $status = 404;
            $content = array_merge(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['error'])) {
                $content['error'] = $status;
            }

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            if (empty($content['message'])) {
                $content['message'] = 'Recurso no encontrado';
            }

            return response()->json($content, $status, $headers);
        });

        Response::macro('jsonForbidden', function ($data = [], $headers = []) {
            $status = 403;
            $content = array_merge(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['error'])) {
                $content['error'] = $status;
            }

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            if (empty($content['message'])) {
                $content['message'] = 'Acceso no autorizado';
            }

            return response()->json($content, $status, $headers);
        });

        Response::macro('jsonException', function ($e, $data = [], $headers = []) {
            $status = $e->getCode() ? $e->getCode() : 500;
            $content = array_merge(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            if (empty($content['error'])) {
                $content['error'] = $e->getCode();
            }

            if (empty($content['message'])) {
                $content['message'] = !empty($e->getMessage()) ? $e->getMessage() : 'Error al procesar los datos';
            }

            if (empty($content['extra'])) {
                $content['extra']['message'] = $e->getMessage();
                //$content['extra']['file'] = $e->getFile();
                //$content['extra']['line'] = $e->getLine();
            }

            return response()->json($content, $content['status'], $headers);
        });

        Response::macro('jsonInvalidData', function ($data = [], $headers = []) {
            $status = 422;
            $content = array_replace(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            if (empty($content['error'])) {
                $content['error'] = 'invalid_data';
            }

            if (empty($content['message'])) {
                $content['message'] = 'Datos incorrectos';
            }

            if (empty($content['extra'])) {
                $content['extra'] = 'Unprocessable Entity';
            }

            return response()->json($content, $status, $headers);
        });

        /**
         *
         * @param Tymon\JWTAuth\Exceptions\JWTException $e
         */
        Response::macro('jsonJwtException', function ($e, $data = [], $headers = []) {
            $status = $e->getStatusCode();
            $content = array_merge(ResponseMacroServiceProvider::$json_format, $data);

            if (empty($content['status'])) {
                $content['status'] = $status;
            }

            if (empty($content['error'])) {
                $content['error'] = $e->getStatusCode();
            }

            if (empty($content['message'])) {
                $content['message'] = $e->getMessage();
            }

            if (empty($content['extra'])) {
                $content['extra'] = $e->getMessage();
            }

            return response()->json($content, $status, $headers);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
