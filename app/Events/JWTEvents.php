<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Eventos lanzados por jwt-auth -> JSON Web Token Authentication
 */
class JWTEvents extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
    * No implementamos el evento valid
    * debido a que solo devuelve el token generado
    */
    public function valid($user)
    {
        
    }

    public function user_not_found()
    {
        return response()->jsonFormat([
            'error' => 404,
            'message' => 'Usuario no encontrado',
            'extra' => 'Not Found',
        ], 404);
    }

    public function invalid($e)
    {
        return response()->jsonFormat([
            'error' => $e->getStatusCode(),
            'message' => 'Token inválido',
            'extra' => $e->getMessage(),
        ], $e->getStatusCode());
    }

    public function expired($e)
    {
        return response()->jsonFormat([
            'error' => $e->getStatusCode(),
            'message' => 'Token expirado',
            'extra' => $e->getMessage(),
        ], $e->getStatusCode());
    }

    public function absent()
    {
        return response()->jsonFormat([
            'error' => 400,
            'message' => 'Token ausente',
            'extra' => 'token_absent',
        ], 400);
    }
}
