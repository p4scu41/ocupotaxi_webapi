<?php

namespace App\Models;

use App\Models\Base;

class Conductor extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conductores';

    /**
     * The primary key column used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'usuario_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario_id',
        'taxi_id',
        'turno',
        'fecha_nacimiento',
        'curp',
        'ine',
        'licencia_conducir',
        'vigencia_licencia_conducir',
        'direccion',
        'colonia',
        'numero',
        'telefono',
        'numero_certificado_aptitud',
        'en_servicio',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules_store = [
        'usuario_id' => 'bail|required|integer|exists:usuarios,id',
        'taxi_id' => 'bail|required|integer|exists:taxis,id',
        'turno' => 'bail|required|max:1',
        'fecha_nacimiento' => 'bail|required|date',
        'curp' => 'bail|required|max:19',
        'ine' => 'bail|required|max:20',
        'licencia_conducir' => 'bail|required|max:20',
        'vigencia_licencia_conducir' => 'bail|required|date',
        'direccion' => 'bail|required|max:100',
        'colonia' => 'bail|required|max:50',
        'numero' => 'bail|required|integer',
        'telefono' => 'bail|required|max:15',
        'numero_certificado_aptitud' => 'bail|required|integer',
        'en_servicio' => 'integer',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'usuario_id' => 'integer|exists:usuarios,id',
        'taxi_id' => 'integer|exists:taxis,id',
        'turno' => 'max:1',
        'fecha_nacimiento' => 'date',
        'curp' => 'max:19',
        'ine' => 'max:20',
        'licencia_conducir' => 'max:20',
        'vigencia_licencia_conducir' => 'date',
        'direccion' => 'max:100',
        'colonia' => 'max:50',
        'numero' => 'integer',
        'telefono' => 'max:15',
        'numero_certificado_aptitud' => 'integer',
        'en_servicio' => 'integer',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function taxi()
    {
        return $this->belongsTo('App\Models\Taxi', 'taxi_id', 'id');
    }
}
