<?php

namespace App\Models;

use App\Models\Base;

class AlertaPanico extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'alertas_panico';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'solicitud_id',
        'latitud',
        'longitud',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'solicitud_id' => 'bail|required|integer|exists:solicitudes,id',
        'latitud' => 'bail|required|max:20',
        'longitud' => 'bail|required|max:20',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'latitud' => 'max:20',
        'longitud' => 'max:20',
        'solicitud_id' => 'integer|exists:solicitudes,id',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * Get the resources related with
     * 
     * @return Model
     */
    public function solicitud()
    {
        return $this->hasOne('App\Models\Solicitud', 'solicitud_id', 'id');
    }
}
