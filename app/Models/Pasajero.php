<?php

namespace App\Models;

use DB;
use App\Models\Base;
use Validator;

class Pasajero extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pasajeros';

    /**
     * The primary key column used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'usuario_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario_id',
        'facebook_id',
        'telefono_celular',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'usuario_id' => 'bail|required|integer|exists:usuarios,id',
        'telefono_cecular' => 'max:15',
        'facebook_id' => 'max:20|unique:pasajeros,facebook_id',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'usuario_id' => 'integer|exists:usuarios,id',
        'telefono_cecular' => 'max:15',
        'facebook_id' => 'max:20',
    ];

    /**
     * Validation rules Facebook
     *
     * @var array
     */
    public static $rules_fb = [
        'facebook_id' => 'bail|required|max:20|unique:pasajeros,facebook_id',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function solicitudes()
    {
        return $this->hasMany('App\Models\Solicitud', 'pasajero_id', 'usuario_id');
    }


    /**
     *  Obtiene todos los pasajeros que tiene solicitudes 
     *  con estado abordado y está en modo pánico
     * 
     * @return Result
     */
    public static function enModoPanico()
    {
        return 
            self::whereHas('solicitudes', function ($query) {
                $query
                    ->has('alertas_panico')
                    ->where('estado', '=', Solicitud::$ESTADO_ABORDADO);
            })
            ->with('usuario')
            //->with('solicitudes')
            ->get();
            /*self::with(['solicitudes' => function ($query) {
                $query->has('alertas_panico');
            }])
            ->get();*/
            /*DB::table('pasajeros')
            ->select([DB::RAW('DISTINCT(pasajeros.usuario_id)'), 'pasajeros.*'])
            ->join('solicitudes', function ($join) {
                $join->on('solicitudes.pasajero_id', '=', 'pasajeros.usuario_id')
                     ->where('solicitudes.estado', '=', Solicitud::$ESTADO_ABORDADO);
            })
            ->join('alertas_panico', 'alertas_panico.solicitud_id', '=', 'solicitudes.id')
            ->get();*/
    }

    /**
     * Obtiene un pasajero a partir de su facebook_id
     * 
     * @param  String $facebook_id
     * @return Result
     */
    public static function getByFb($facebook_id)
    {
        return self::with('usuario')
            ->where('facebook_id', $facebook_id)
            /*->join('usuarios', function ($join) {
                $join->on('usuarios.id', '=', 'pasajeros.usuario_id')
                     ->where('usuarios.activo', 1);
            })*/
            ->first();
    }

    /**
     * Registra un pasajero con su facebook_id
     * 
     * @param  Array $data Datos a registrar
     * @return Result
     */
    public static function registerByFb($data)
    {
        // Establecemos el rol como pasajero
        $data['rol_id'] = User::$ROL_PASAJERO;
        // Por defecto, el pasajero se registra como activo
        $data['activo'] = 1;

        // Validamos los datos del usuario
        $validator = Validator::make($data, User::$rules_fb, User::$messages);
        if ($validator->fails()) {
            throw new \Exception($validator->errors(), 422);
        }
        $user = User::create($data);

        $data['usuario_id'] = $user->id;

        // Validamos los datos del pasajero
        $validator = Validator::make($data, Pasajero::$rules_fb, Pasajero::$messages);
        if ($validator->fails()) {
            throw new \Exception($validator->errors(), 422);
        }

        Pasajero::create($data);
        // Por alguna extraña razón no devuelve los datos insertados en la tabla
        // por lo que se procede a obtener el registro del pasajero
        $pasajero = Pasajero::find($user->id);
        $pasajero->load('usuario');

        return $pasajero->toArray();
    }
}
