<?php

namespace App\Models;

use GuzzleHttp\Client;

/**
 *
 * Tags utilizados para realizar las notificaciones
 *
 *     - Conductores
 *     municipio_id = #
 *     conductor_id = #
 *
 *     - Pasajeros
 *     pasajero_id = #
 *
 * Contenido en Data
 *     tipo_push
 *     1 = Solicitud de nuevo taxi (Enviado sólo a conductores)
 *     2 = Asignación de taxi (Enviado sólo a pasajero que realizó la solicitud)
 *     3 = Servicio cancelado por conductor
 *     4 = Servicio cancelado por pasajero
 *     5 = Servicio terminado por conductor
 *     6 = Pasajero a bordo
 *     7 = Asignación de taxi (Enviado sólo a conductor que tomó el servicio)
 */
class OneSignal
{
    public static $TIPO_PUSH_SOLICITUD_TAXI = 1;
    public static $TIPO_PUSH_ASIGNACION_TAXI = 2;
    public static $TIPO_PUSH_CONDUCTOR_CANCELA_SERVICIO = 3;
    public static $TIPO_PUSH_PASAJERO_CANCELA_SERVICIO = 4;
    public static $TIPO_PUSH_CONDUCTOR_FINALIZA_SERVICIO = 5;
    public static $TIPO_PUSH_PASAJERO_A_BORDO = 6;
    public static $TIPO_PUSH_ASIGNACION_TAXI_CONDUCTOR = 7;

    /**
     * Envía notificación PUSH con OneSignal
     *
     * $contenido array Arreglo hash de contenido de mensaje con un hash por cada idioma. NO se usa cuando $enFondo = true. Ej: ['en'=>'Content', 'es'=>'Contenido', 'fr'=>'contenu')
     * $data array Arreglo hash de información que la APP puede usar para algún proceso interno.
     * $tags array Arreglo de arreglos especificando tags para filtrar destinatarios. NO se usa si $oneSignalPlayerIds existe. Ej: [ ['key'=>'ciudad','relation'=>'=','value'=>'tuxtla'), ['key'=>'sexo','relation'=>'=','value'=>'f')   )
     * $oneSignalPlayerIds array Lista de strings con player ids (dispositivos registrados) de onesignal ej: ['id1', 'id2']
     * $enFondo bool Indica si push debe ser procesado en fondo por aplicación sin que usuario sea notificado.
     */
    public static function enviarPush($contenido, $data=NULL, $tags=NULL, $oneSignalPlayerIds=NULL, $enFondo = true)
    {
        $appId = '76bcf1ff-cbb5-4fec-b9b8-85c8fb408531';
        $apiKey = 'MTdjM2UzOTMtOTI3Ni00OGFiLTk4ODUtN2Y3NDAxMTI5NTc0';

        $fields = [
            'app_id' => $appId,
        ];

        if( !empty($data) ) $fields['data'] = $data;

        if ($enFondo) {
            $fields['android_background_data'] = true; //para activar fondo en android
            $fields['content_available'] = true; //para activar fondo en ios
        } else {
            $fields['contents'] = $contenido;
        }

        if( empty($oneSignalPlayerIds) ) { //Se puede usar tags y segmentos
            $fields['included_segments'] = ['All'];

            if( !empty($tags) ) $fields['tags'] = $tags;
        } else {
            $fields['include_player_ids'] = $oneSignalPlayerIds;
        }

        $fields = json_encode($fields);
        //error_log('OneSignal JSON Sent: '.$fields);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
           'Authorization: Basic ' . $apiKey ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        // Si encuentra algún error, lanza una excepción con error 500
        if ( isset($response->errors) ) {
            // errors == 'All included players are not subscribed'
            if ($response->recipients == 0) {
                 throw new \Exception('No se encontraron dispositivos para notificar.', 404);
            }

            throw new \Exception('OneSignal Error: '.implode($response->errors, ', '), 500);
        }

        return $response;
    }

    /**
     * Envia notificaciones push
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarSolicitud($notificacion = [])
    {
        $contenido = [
            'en' => $notificacion['contents'],
            'es' => $notificacion['contents'],
        ];
        $datosExtra = isset($notificacion['data']) ? $notificacion['data'] : [];
        $tags = $notificacion['tags'];
        $playerIds = isset($notificacion['player_ids']) ? $notificacion['player_ids'] : [];;
        $enFondo = true;

        $response = self::enviarPush($contenido, $datosExtra, $tags, $playerIds, $enFondo);

        //error_log('OneSignal JSON Received: '.json_encode($response));

        return $response;
    }

    /**
     * Envia notificaciones push a los conductores cuando se recibe una nueva solicitud
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarSolicitudConductores($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'Nueva solicitud de Taxi';
        }
        // Respalda el listado de tags a enviar
        // las tags se reciben como array clave => valor
        // valor es un array
        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Nueva solicitud de Taxi
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_SOLICITUD_TAXI;

        // Recibe todas los tags como un array clave => valor
        // donde valor es un array con todos los posibles valores del tag
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $tag) {
                // Recorremos los valores de cada tag
                foreach ($tag as $value) {
                    $notificacion['tags'][] = [
                        'key' => $key,
                        'relation' => '=',
                        'value' => $value,
                    ];
                    // Agregar el operador OR
                    $notificacion['tags'][] = [
                        'operator' => 'OR'
                    ];
                }
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }

    /**
     * Envia notificaciones push a los pasajeros cuando su solicitud ha sido procesada
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarSolicitudPasajero($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'Su solicitud de Taxi ha sido procesada';
        }

        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Asignación de taxi
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_ASIGNACION_TAXI;

        // Recibe todas los tags como un array clave => valor
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $value) {
                $notificacion['tags'][] = [
                    'key' => $key,
                    'relation' => '=',
                    'value' => $value,
                ];
                // Agregar el operador OR
                $notificacion['tags'][] = [
                    'operator' => 'OR'
                ];
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }

    /**
     * Envia notificación push al conductor que ha tomado el servicio
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarAsignacionServicioConductor($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'Su solicitud de Taxi ha sido procesada';
        }

        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Asignación de taxi
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_ASIGNACION_TAXI_CONDUCTOR;

        // Recibe todas los tags como un array clave => valor
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $value) {
                $notificacion['tags'][] = [
                    'key' => $key,
                    'relation' => '=',
                    'value' => $value,
                ];
                // Agregar el operador OR
                $notificacion['tags'][] = [
                    'operator' => 'OR'
                ];
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }

    /**
     * Envía push a pasajero cuando un conductor ha cancelado la solicitud
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarCancelacionAlPasajero($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'Su solicitud de Taxi ha sido cancelada';
        }

        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Asignación de taxi
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_CONDUCTOR_CANCELA_SERVICIO;

        // Recibe todas los tags como un array clave => valor
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $value) {
                $notificacion['tags'][] = [
                    'key' => $key,
                    'relation' => '=',
                    'value' => $value,
                ];
                // Agregar el operador OR
                $notificacion['tags'][] = [
                    'operator' => 'OR'
                ];
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }

    /**
     * Envía push a conductor cuando un pasajero ha cancelado la solicitud
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarCancelacionAlConductor($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'El pasajero ha cancelado el servicio';
        }

        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Asignación de taxi
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_PASAJERO_CANCELA_SERVICIO;

        // Recibe todas los tags como un array clave => valor
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $value) {
                $notificacion['tags'][] = [
                    'key' => $key,
                    'relation' => '=',
                    'value' => $value,
                ];
                // Agregar el operador OR
                $notificacion['tags'][] = [
                    'operator' => 'OR'
                ];
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }

    /**
     * Envía push a pasajero cuando conductor declara fin del servicio
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarFinServicio($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'El conductor ha dado por finalizado el servicio';
        }

        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Asignación de taxi
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_CONDUCTOR_FINALIZA_SERVICIO;

        // Recibe todas los tags como un array clave => valor
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $value) {
                $notificacion['tags'][] = [
                    'key' => $key,
                    'relation' => '=',
                    'value' => $value,
                ];
                // Agregar el operador OR
                $notificacion['tags'][] = [
                    'operator' => 'OR'
                ];
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }

    /**
     * Envia notificación push al pasajero cuando el conductor indica que el pasajero esta a bordo
     *
     * @param  Array $notificacion
     * @return Array Respuesta devuelta por One Signal
     */
    public static function notificarPasajeroABordo($notificacion = [])
    {
        if ( !isset($notificacion['contents']) ) {
            $notificacion['contents'] = 'Pasajero a bordo';
        }

        $tags = isset($notificacion['tags']) ? $notificacion['tags'] : [];

        $notificacion['tags'] = [];
        // Pasajero a bordo
        $notificacion['data']['tipo_push'] = OneSignal::$TIPO_PUSH_PASAJERO_A_BORDO;

        // Recibe todas los tags como un array clave => valor
        // anexa el parámetro tags con el formato adecuado
        if (count($tags)) {
            // Recorremos todos los tags a enviar
            foreach ($tags as $key => $value) {
                $notificacion['tags'][] = [
                    'key' => $key,
                    'relation' => '=',
                    'value' => $value,
                ];
                // Agregar el operador OR
                $notificacion['tags'][] = [
                    'operator' => 'OR'
                ];
            }

            // Eliminamos el último operador OR
            array_pop($notificacion['tags']);
        }

        return self::notificarSolicitud($notificacion);
    }
}
