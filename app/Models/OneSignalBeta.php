<?php

namespace App\Models;

use GuzzleHttp\Client;

class OneSignalBeta
{
    public static $base_uri = 'https://onesignal.com/api/v1/';//'http://localhost/ocupotaxi_webapi/public/api/v1/';
    public static $app_id = '8f7a98dd-2636-42be-ab36-3668aa8dcb06';
    public static $rest_api_key = 'Y2I3ZDgyNWMtNGE3NS00YTEyLWIzOTMtMGQzYjRjYjNkMzRm';

    public static function sendPush($data)
    {
        $notificacion = [
            'app_id' => self::$app_id,
            'included_segments' => ['All'],
            /*'tags' => [
                [
                    'key' => 'level', 
                    'relation' => '>', 
                    'value' => '10'
                ]
            ]*/
            //'data' => [],
            //'isAndroid' => true,
            'headings' => [
                'en' => $data['headings'],
                'es' => $data['headings'],
            ],
            'contents' => [
                'en' => $data['contents'],
                'es' => $data['contents'],
            ],
        ];

        $client = new Client([
            'base_uri' => self::$base_uri,
            //'verify' => false, // Solo para pruebas y desarrollo se deshabilita la verificación de SSL
        ]);

        $response = $client->request('POST', 'notifications', [ //admin/pasajeros
            'headers' => [
                // 'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => 'Basic '.self::$rest_api_key,
            ],
            'json' => $notificacion,
        ]);

        $respArray = [
            'code' => $response->getStatusCode(),
            'status' => $response->getReasonPhrase(),
            'content' => json_decode($response->getBody()), //->getContents()
            'headers' => [],
        ];

        foreach ($response->getHeaders() as $name => $values) {
            $respArray['headers'][$name] = implode(', ', $values);
        }

        if ( isset($respArray['content']->errors) ) {
            throw new \Exception('OneSignal Error: '.implode($respArray['content']->errors, ', '), 500);
        }

        return $respArray;
    }

    public static function sendPushNuevaSolicitud()
    {
        return self::sendPush([
            'headings' => 'Nueva solicitud de Taxi',
            'contents' => 'Revise su bandeja de entrada, existe una nueva solicitud de Taxi',
        ]);
    }
}
