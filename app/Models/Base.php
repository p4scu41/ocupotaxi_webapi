<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Permite crear where anidados a partir de un array asociativo
     * 
     * @param  Array $params Array asociativo con campo y valor
     * @return Query
     */
    public static function whereArray($params)
    {
        if (count($params)) {
            $where = each($params);
            $result = self::where($where['key'], $where['value']);

            while (list($campo, $valor) = each($params)) {
                $result = self::where($campo, $valor);
            }

            return $result;
        }

        return null;
    }
}
