<?php

namespace App\Models;

use App\Models\Base;

class Taxi extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'taxis';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'concesionario_id',
        'modalidad_id',
        'tipo',
        'marca',
        'modelo',
        'numero_motor',
        'numero_serie',
        'numero_economico',
        'tarjeta_circulacion',
        'placas',
        'capacidad',
        'factura',
        'aseguradora_id',
        'poliza_seguro',
        'vigencia_poliza',
        'activo',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'concesionario_id' => 'bail|required|integer|exists:concesionario,id',
        'modalidad_id' => 'bail|required|integer|exists:modalidades,id',
        'tipo' => 'bail|required|max:100',
        'marca' => 'bail|required|max:50',
        'modelo' => 'bail|required|max:4',
        'numero_motor' => 'bail|required|max:25|unique:taxis',
        'numero_serie' => 'bail|required|max:25|unique:taxis',
        'numero_economico' => 'bail|required|max:4|unique:taxis',
        'tarjeta_circulacion' => 'bail|required|max:10|unique:taxis',
        'placas' => 'bail|required|max:7|unique:taxis',
        'capacidad' => 'bail|required|integer',
        'factura' => 'bail|required|max:50|unique:taxis',
        'aseguradora_id' => 'bail|required|integer|exists:aseguradoras,id',
        'poliza_seguro' => 'bail|required|max:50',
        'vigencia_poliza' => 'bail|required|date',
        'activo' => 'integer',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'concesionario_id' => 'bail|integer|exists:concesionario,id',
        'modalidad_id' => 'bail|integer|exists:modalidades,id',
        'tipo' => 'bail|max:100',
        'marca' => 'bail|max:50',
        'modelo' => 'bail|max:4',
        'numero_motor' => 'bail|max:25',
        'numero_serie' => 'bail|max:25',
        'numero_economico' => 'bail|max:4',
        'tarjeta_circulacion' => 'bail|max:10',
        'placas' => 'bail|max:7',
        'capacidad' => 'bail|integer',
        'factura' => 'bail|max:50',
        'aseguradora_id' => 'bail|integer|exists:aseguradoras,id',
        'poliza_seguro' => 'bail|max:50',
        'vigencia_poliza' => 'bail|date',
        'activo' => 'integer',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function concesion()
    {
        return $this->belongsTo('App\Models\Concesion', 'concesionario_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function aseguradora()
    {
        return $this->belongsTo('App\Models\Aseguradora', 'aseguradora_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function conductores()
    {
        return $this->hasMany('App\Models\Conductor', 'taxi_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function modalidad()
    {
        return $this->belongsTo('App\Models\Modalidad', 'modalidad_id', 'id');
    }
}
