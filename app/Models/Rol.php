<?php

namespace App\Models;

use App\Models\Base;

class Roles extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'bail|required|max:100',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'nombre' => 'bail|required|max:100',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * Get the resources related with
     * 
     * @return Model
     */
    public function taxis()
    {
        return $this->hasMany('App\Models\Taxis', 'aseguradora_id', 'id');
    }
}
