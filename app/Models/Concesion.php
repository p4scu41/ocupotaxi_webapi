<?php

namespace App\Models;

use App\Models\Base;

class Concesion extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'concesionario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organizacion',
        'numero_concesion',
        'nombre_concesionado',
        'nombre_representante_legal',
        'domicilio',
        'municipio_id',
        'localidad',
        'telefono',
        'fecha_concesion',
        'acta_constitutiva',
        'periodico_oficial',
        'fecha_publicacion',
        'ine',
        'telefono_casa',
        'telefono_celular',
        'activo',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'organizacion' => 'max:20',
        'numero_concesion' => 'bail|required|max:50|unique:concesionario',
        'nombre_concesionado' => 'bail|required|max:100',
        'nombre_representante_legal' => 'bail|required|max:100',
        'domicilio' => 'bail|required|max:200',
        'municipio_id' => 'bail|required|integer|exists:municipios,id',
        'localidad' => 'bail|required|max:100',
        'telefono' => 'bail|required|max:15',
        'fecha_concesion' => 'bail|required|date',
        'acta_constitutiva' => 'bail|required|max:50',
        'periodico_oficial' => 'bail|required|max:50',
        'fecha_publicacion' => 'bail|required|date',
        'ine' => 'bail|required|max:18',
        'telefono_casa' => 'bail|required|max:15',
        'telefono_celular' => 'bail|required|max:15',
        'activo' => 'integer',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'organizacion' => 'max:20',
        'numero_concesion' => 'bail|max:50',
        'nombre_concesionado' => 'bail|max:100',
        'nombre_representante_legal' => 'bail|max:100',
        'domicilio' => 'bail|max:200',
        'municipio_id' => 'bail|integer|exists:municipios,id',
        'localidad' => 'bail|max:100',
        'telefono' => 'bail|max:15',
        'fecha_concesion' => 'bail|date',
        'acta_constitutiva' => 'bail|max:50',
        'periodico_oficial' => 'bail|max:50',
        'fecha_publicacion' => 'bail|date',
        'ine' => 'bail|max:18',
        'telefono_casa' => 'bail|max:15',
        'telefono_celular' => 'bail|max:15',
        'activo' => 'integer',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * The resource related with
     *
     * @return Model
     */
    public function municipio()
    {
        return $this->belongsTo('App\Models\Municipio', 'municipio_id', 'id');
    }

    /**
     * The resource related with
     *
     * @return Model
     */
    public function taxis()
    {
        return $this->hasMany('App\Models\Taxi', 'concesion_id', 'id');
    }
}
