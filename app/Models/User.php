<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;
use Image;

class User extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuarios';

    public static $ROL_ADMINISTRADOR = 1;
    public static $ROL_CONDUCTOR = 2;
    public static $ROL_PASAJERO = 3;
    public static $ROL_MONITOR = 4;

    /**
     * Se agrega la variable foto, manipulada por los accessors
     * al array de atributos
     */
    // protected $appends = ['foto'];
    public $foto = null;

    /**
     * Ruta donde se guardan las fotos
     * @var string
     */
    public $dir_foto = '/storage/app/fotos';

    /**
     * Dependiendo del tipo de usuario es la carpeta donde se guarda
     * @var string
     */
    public $folder_foto = [
        1 => 'administrador',
        2 => 'conductor',
        3 => 'pasajero',
        4 => 'monitor',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rol_id',
        'email',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'municipio_id',
        'sexo',
        'avatar',
        'password',
        'remember_token',
        'token_tracking',
        'activo',
        'foto',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token_tracking',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rol_id' => 'bail|required|integer|exists:roles,id',
        'email' => 'bail|required|max:50|email|unique:usuarios',
        'nombre' => 'bail|required|max:50',
        'apellido_paterno' => 'bail|required|max:50',
        'apellido_materno' => 'bail|required|max:50',
        'municipio_id' => 'bail|integer|exists:municipios,id',
        'sexo' => 'bail|max:1', // |required|in:M,F
        'password' => 'required',
        'activo' => 'integer',
        'foto' => 'bail|image|max:2048', // Solo imagen, máximo 2MB 
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'rol_id' => 'integer|exists:roles,id',
        'email' => 'max:50|email',
        'nombre' => 'max:50',
        'apellido_paterno' => 'max:50',
        'apellido_materno' => 'max:50',
        'municipio_id' => 'integer|exists:municipios,id',
        'sexo' => 'max:1', // |in:M,F
        'foto' => 'bail|image|max:2048', // Solo imagen, máximo 2MB 
    ];

    /**
     * Validation rules Facebook
     *
     * @var array
     */
    public static $rules_fb = [
        'rol_id' => 'bail|required|integer|exists:roles,id',
        'email' => 'bail|required|max:50|email|unique:usuarios',
        'nombre' => 'bail|required|max:50',
        'foto' => 'bail|image|max:2048', // Solo imagen, máximo 2MB 
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [
        'sexo.in' => 'sexo inválido, debe ser M (Masculino) ó  F (Femenino)',
    ];

    public function isAdministrador()
    {
        return $this->rol_id == self::$ROL_ADMINISTRADOR;
    }

    public function isPasajero()
    {
        return $this->rol_id == self::$ROL_PASAJERO;
    }

    public function isConductor()
    {
        return $this->rol_id == self::$ROL_CONDUCTOR;
    }

    /**
     * Verifica si el usuario tiene el rol especificado
     * 
     * @param  integer  $rol Roles
     * @return boolean
     */
    public function isRol($rol)
    {
        return $this->rol_id == $rol;
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function conductor()
    {
        return $this->hasOne('App\Models\Conductor', 'usuario_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function pasajero()
    {
        return $this->hasOne('App\Models\Pasajero', 'usuario_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function rol()
    {
        return $this->hasOne('App\Models\Rol', 'rol_id', 'id');
    }

    /**
     * The resource related with
     * 
     * @return Model
     */
    public function municipio()
    {
        return $this->belongsTo('App\Models\Municipio', 'municipio_id', 'id');
    }

    // Accessors para la variable foto
    public function setFotoAttribute($value)
    {
        //$this->attributes['foto'] = $value;
        $this->foto = $value;
    }

    public function getFotoAttribute()
    {
        //return $this->attributes['foto'];
        return $this->foto;
    }
    
    public function getFolderFoto()
    {
        return base_path() . $this->dir_foto . DIRECTORY_SEPARATOR .
            $this->folder_foto[$this->rol_id];
    }

    public function uploadFoto($request, $param='foto')
    {
        $pathFoto = $this->getFolderFoto();
        $file = $request->file($param);

        if (!empty($file)) {
            if (!$file->isValid()) {
                throw new \Exception('La imagen seleccionada excede el tamaño máximo permitido (2MB).');
            }

            $validator = Validator::make($request->all(), ['foto' => 'bail|image|max:2048']);

            if ($validator->fails()) {
                throw new \Exception($validator->errors()->first('foto'), 422);
            }

            $file->move($pathFoto, $this->id.'.jpg');

            // Se utiliza la librería Intervention Image 
            Image::make($pathFoto. DIRECTORY_SEPARATOR . $this->id.'.jpg')
                ->heighten(150) // Se redimensiona la imagen a un alto de 150, automaticamente se calcula el ancho
                ->save($pathFoto. DIRECTORY_SEPARATOR . $this->id.'.jpg', 80); // se guarda con la calidad de 80%

            return true;
        }

        return false;
    }
    
    public function getPathFoto()
    {
        return $this->getFolderFoto() . DIRECTORY_SEPARATOR . $this->id.'.jpg';
    }

    public function parseFoto()
    {
        $pathFoto = $this->getPathFoto();
        $strFoto = null;

        // Verifica si existe alguna foto asociada con el conductor
        if (file_exists($pathFoto)) {
            $typeFoto = pathinfo($pathFoto, PATHINFO_EXTENSION);
            $byteFoto = file_get_contents($pathFoto);
            // Convierte la foto en codificación base64 (Data URI Scheme)
            $strFoto = 'data:image/' . $typeFoto . ';base64,' . base64_encode($byteFoto);
        }

        $this->foto = $strFoto;

        return $strFoto;
    }
    
    public function getUrlFoto()
    {
        return str_replace('public', 'public/api/v1', url('usuarios/foto', [$this->id]));
    }
}
