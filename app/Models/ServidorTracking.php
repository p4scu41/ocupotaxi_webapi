<?php

namespace App\Models;

use GuzzleHttp\Client;

class ServidorTracking
{
    public static $base_uri = 'https://ocupotaxi.herokuapp.com';

    public static function obtenerToken($data)
    {
        $client = new Client([
            'base_uri' => self::$base_uri,
        ]);

        // Este valor es fijo, establecido por el servidor
        $data['key'] = 'OUOAICPTX';
        // El tiempo se comparte con el servidor tracking
        // para coincidir con los tiempos de expiración
        $data['expiracion'] = config('jwt.refresh_ttl');
        
        $response = $client->request('POST', 'obtener-token', [
            'json' => $data,
        ]);

        $respArray = [
            'code' => $response->getStatusCode(),
            'status' => $response->getReasonPhrase(),
            'content' => json_decode($response->getBody()),
            'headers' => [],
        ];

        foreach ($response->getHeaders() as $name => $values) {
            $respArray['headers'][$name] = implode(', ', $values);
        }

        if ( isset($respArray['content']->errors) ) {
            throw new \Exception('Servidor Tracking Error: '.implode($respArray['content']->errors, ', '), 500);
        }

        return $respArray['content']->token;
    }

    public static function get($data)
    {
        $client = new Client([
            'base_uri' => self::$base_uri,
        ]);

        /*$response = $client->request('GET', 'notifications', [
            'query' => $data,
        ]);

        $respArray = [
            'code' => $response->getStatusCode(),
            'status' => $response->getReasonPhrase(),
            'content' => json_decode($response->getBody()),
            'headers' => [],
        ];

        foreach ($response->getHeaders() as $name => $values) {
            $respArray['headers'][$name] = implode(', ', $values);
        }

        if ( isset($respArray['content']->errors) ) {
            throw new \Exception('Servidor Tracking Error: '.implode($respArray['content']->errors, ', '), 500);
        }

        return $respArray;*/
    }
}
