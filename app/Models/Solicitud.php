<?php

namespace App\Models;

use DB;
use App\Models\Base;

class Solicitud extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'solicitudes';

    public static $ESTADO_NUEVO     = 1;
    public static $ESTADO_ASIGNADO  = 2;
    public static $ESTADO_ABORDADO  = 3;
    public static $ESTADO_TERMINADO = 4;
    public static $ESTADO_CANCELADO = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estado',
        'pasajero_id',
        'conductor_id',
        'fecha_abordaje',
        'fecha_arribo_destino',
        'calificacion',
        'tiempo_llegada',
        'latitud',
        'longitud',
        'detalles',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'estado' => 'bail|required|integer',
        'pasajero_id' => 'bail|required|integer|exists:pasajeros,usuario_id',
        'conductor_id' => 'bail|integer|exists:conductores,usuario_id',
        'fecha_abordaje' => 'datetime',
        'fecha_arribo_destino' => 'datetime',
        'calificacion' => 'integer',
        'tiempo_llegada' => 'date_format:H:i',
        'latitud' => 'bail|required|numeric',
        'longitud' => 'bail|required|numeric',
        'detalles' => 'max:100',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'estado' => 'integer',
        'pasajero_id' => 'integer|exists:pasajeros,usuario_id',
        'conductor_id' => 'exists:conductores,usuario_id',
        'fecha_abordaje' => 'datetime',
        'fecha_arribo_destino' => 'datetime',
        'calificacion' => 'integer',
        'tiempo_llegada' => 'date_format:H:i',
        'latitud' => 'numeric',
        'longitud' => 'numeric',
        'detalles' => 'max:100',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * Get the resources related with
     *
     * @return Model
     */
    public function conductor()
    {
        return $this->hasOne('App\Models\Conductor', 'usuario_id', 'conductor_id');
    }

    /**
     * The resource related with
     *
     * @return Model
     */
    public function pasajero()
    {
        return $this->hasOne('App\Models\Pasajero', 'usuario_id', 'pasajero_id');
    }

    /**
     * Get the resources related with
     *
     * @return Model
     */
    public function alertas_panico()
    {
        return $this->hasMany('App\Models\AlertaPanico', 'solicitud_id', 'id');
    }

    /**
     * Devuelve las solicitudes con estado abordo y que estan en modo pánico
     * de un pasajero especifico
     *
     * @param  [type] $pasajero_id [description]
     * @return Query
     */
    public static function enModoPanicoFromPasajero($pasajero_id)
    {
        return
            self::where('estado', Solicitud::$ESTADO_ABORDADO)
            ->where('pasajero_id', $pasajero_id)
            ->has('alertas_panico')
            ->with('alertas_panico')
            ->get();;
    }


    public function getMunicipiosNotificar()
    {
        /*$query = 'SELECT
                municipios.*,
                ROUND(distanciaPuntosGps(latitud, longitud, '.$this->latitud.','.$this->longitud.')) AS distancia
            FROM municipios
            HAVING distancia <= radio
            ORDER BY distancia';*/

        $municipios_id = [];
        $result = DB::table('municipios')
            ->select([
                DB::RAW('municipios.*'),
                DB::RAW('ROUND(distanciaPuntosGps(latitud, longitud, '.$this->latitud.', '.$this->longitud.')) AS distancia')
            ])
            ->havingRaw('distancia <= radio')
            ->orderBy('distancia', 'asc')
            ->get();

        if (count($result)) {
            // Obtenemos solo el id del municipio
            $municipios_id = collect($result)->map(function ($municipio) {
                return $municipio->id;
            });
        }

        return $municipios_id;
    }

    /**
     *
     *
     * @param  array $params [description]
     * @return array organizacion, fecha_abordaje, concesionario_id
     */
    public static function reporte($params)
    {
        $query = 'SELECT
            concesionario.organizacion,
            concesionario.nombre_concesionado,
            taxis.numero_economico,
            taxis.placas,
            CONCAT(usuarios.nombre,\' \',usuarios.apellido_paterno,\' \',usuarios.apellido_materno) AS conductor,
            (SELECT
                COUNT(id)
            FROM
                solicitudes
            WHERE
                estado = '.self::$ESTADO_TERMINADO.'
                AND conductor_id = conductores.usuario_id'.
                (isset($params['fecha_abordaje']) ? ' AND fecha_abordaje>=\''.$params['fecha_abordaje'].' 00:00:00\' ' : '').') AS servicios_atendidos,
            (SELECT
                COUNT(id)
            FROM
                solicitudes
            WHERE
                estado = '.self::$ESTADO_CANCELADO.'
                AND conductor_id = conductores.usuario_id'.
                (isset($params['fecha_abordaje']) ? ' AND fecha_abordaje>=\''.$params['fecha_abordaje'].' 00:00:00\' ' : '').') AS servicios_cancelados
        FROM
            concesionario
        INNER JOIN taxis ON
            taxis.concesionario_id = concesionario.id
        INNER JOIN conductores ON
            conductores.taxi_id = taxis.id
        INNER JOIN usuarios ON
            usuarios.id = conductores.usuario_id
        WHERE 1 = 1 '.
            (isset($params['organizacion']) ? ' AND concesionario.organizacion = \''.$params['organizacion'].'\' ' : '').
            (isset($params['concesionario_id']) ? ' AND concesionario.id = '.$params['concesionario_id'].' ' : '').
        'ORDER BY
            concesionario.organizacion, concesionario.nombre_concesionado, taxis.numero_economico';

        // Devolvemos la lista de objetos como un array
        return DB::select($query);
    }

    // https://css-tricks.com/snippets/php/generate-csv-from-array/
    public static function arrayToCsv($data, $delimiter=',', $enclosure='"')
    {
        $handle = fopen('php://temp', 'r+');
        $contents = '';

        foreach ($data as $line) {
            fputcsv($handle, collect($line)->map(function ($cel) { return mb_convert_encoding($cel, 'ISO-8859-2', 'UTF-8'); })->all(), $delimiter, $enclosure);
        }

        rewind($handle);

        while (!feof($handle)) {
            $contents .= fread($handle, 8192);
        }

        fclose($handle);

        return $contents;
    }

    public static function lockTable()
    {
        /**
         * http://stackoverflow.com/questions/17434102/causes-of-mysql-error-2014-cannot-execute-queries-while-other-unbuffered-queries
         * https://github.com/laravel/framework/issues/1761
         *
         * Se ejecuta de forma independiente la sentencia porque de lo contrario se obtiene el siguiente error
         * SQLSTATE[HY000]: General error: 2014 Cannot execute queries while other unbuffered queries are active.
         * Consider using PDOStatement::fetchAll(). Alternatively, if your code is only ever going to run against
         * mysql, you may enable query buffering by setting the PDO::MYSQL_ATTR_USE_BUFFERED_QUERY attribute.
         * (SQL: UNLOCK TABLES)
         */
        DB::raw('LOCK TABLES solicitudes WRITE');
        //DB::statement('LOCK TABLES solicitudes WRITE');
    }

    public static function unlockTable()
    {
        /**
         * http://stackoverflow.com/questions/17434102/causes-of-mysql-error-2014-cannot-execute-queries-while-other-unbuffered-queries
         * https://github.com/laravel/framework/issues/1761
         *
         * Se ejecuta de forma independiente la sentencia porque de lo contrario se obtiene el siguiente error
         * SQLSTATE[HY000]: General error: 2014 Cannot execute queries while other unbuffered queries are active.
         * Consider using PDOStatement::fetchAll(). Alternatively, if your code is only ever going to run against
         * mysql, you may enable query buffering by setting the PDO::MYSQL_ATTR_USE_BUFFERED_QUERY attribute.
         * (SQL: UNLOCK TABLES)
         */
        DB::raw('UNLOCK TABLES');
        //DB::statement('UNLOCK TABLES');
    }

    /**
     * Se sobrescribre el metodo save para realizar las validaciones de
     * si el conductor no tiene un servicio activo o
     * si el servicio ya ha sido tomado por otro conductor
     *
     * @param  boolean $tomar_servicio Activado cuando se esta ejecutando la acción de tomar el servicio por parte del conductor
     * @throw Exception
     */
    public function checkAndSave($tomar_servicio = false)
    {
        // Si el conductor esta tomando el servicio
        // se debe validar que el servicio no haya sido
        // tomado por otro conductor o que el conductor
        // no tenga algun servicio activo
        if ($tomar_servicio) {
            // Primero validamos que el conductor no tenga algun servicio activo
            $servicioConductor = DB::select('SELECT id FROM solicitudes WHERE
                conductor_id = ? AND (estado = '.self::$ESTADO_ASIGNADO.' OR
                estado = '.self::$ESTADO_ABORDADO.')', [$this->conductor_id]);

            // Si la consulta devolvio algun servicio activo
            if (count($servicioConductor)) {
                $servicioConductor = $servicioConductor[0];

                // El conductor tiene un servicio activo por lo tanto
                // lanzamos la excepcion 409 Conflict con el id del servicio activo
                throw new \Exception($servicioConductor->id, 409);
            }

            // Ahora validamos que la solicitud no haya sido tomado por otro conductor
            $solicitud = DB::select('SELECT * FROM solicitudes WHERE id = ?', [$this->id]);
            $solicitud = $solicitud[0];

            // Si la solicitud ya ha sido tomada por alguien mas
            // se lanza el error 403 Forbidden
            if (!empty($solicitud->conductor_id)) {
                throw new \Exception('El Servicio ya esta asignado', 403);
            }
        }

        return $this->save();
    }
}
