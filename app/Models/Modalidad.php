<?php

namespace App\Models;

use App\Models\Base;

class Modalidad extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modalidades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'bail|required|max:100',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'nombre' => 'bail|required|max:100',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * Get the resources related with
     * 
     * @return Model
     */
    public function taxis()
    {
        return $this->hasMany('App\Models\Taxi', 'modalidad_id', 'id');
    }
}
