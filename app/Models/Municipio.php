<?php

namespace App\Models;

use App\Models\Base;

class Municipio extends Base
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'municipios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clave',
        'nombre',
        'latitud',
        'longitud',
        'radio',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rules = [
        'clave' => 'bail|required|max:4',
        'nombre' => 'bail|required|max:100',
    ];

    /**
     * Validation rules on update
     *
     * @var array
     */
    public static $rules_update = [
        'clave' => 'max:4',
        'nombre' => 'max:100',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [];

    /**
     * Get the resources related with
     * 
     * @return Model
     */
    public function concesiones()
    {
        return $this->hasMany('App\Models\Concesion', 'municipio_id', 'id');
    }
}
