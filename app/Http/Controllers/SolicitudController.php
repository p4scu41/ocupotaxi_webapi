<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Solicitud;
use App\Models\OneSignal;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use Validator;
use JWTAuth;
use DB;

class SolicitudController extends Controller
{
    // Guarda la entidad que se solicita cuando se ejecuta las rutas:
    // show, edit, update, destroy
    public $modelInst = null;

    // Nombre del recurso que se expone en las rutas
    public $resource = 'solicitudes';

    function __construct(Route $route)
    {
        $this->find($route);
    }

    /**
     * Get the resource with the specific ID
     * Se ejecuta cuando se solicitan las rutas:
     * show, edit, update, destroy
     *
     * @param  Route  $route
     * @return void   Redirect when the resource is not found
     */
    public function find(Route $route)
    {
        // Relaciones con la tabla Solicitud, para incluir los datos en la respuesta
        $with = [
            'conductor' => function ($query) {
                $query->with(['usuario','taxi']);
            },
            'pasajero' => function ($query) {
                $query->with('usuario');
            }
        ];

        try {
            // Si la solicitud se hizo desde el conductor
            if (stripos(\Illuminate\Support\Facades\Route::currentRouteName(), 'conductor') !== false) {
                // Si realmente la petición se realizó desde el conductor
                // Eliminamos el dato del conductor del with, porque no es necesario
                // ya que los datos del conductor se pueden obtener con otro servicio
                unset($with['conductor']);
            }

            // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
            $id = $route->getParameter($this->resource) ? $route->getParameter($this->resource) : $route->getParameter('id');
            $this->modelInst = Solicitud::with($with)->find($id);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->input();
        // Relaciones con la tabla Solicitud, para incluir los datos en la respuesta
        $with = [
            'conductor' => function ($query) {
                $query->with(['usuario','taxi']);
            },
            'pasajero' => function ($query) {
                $query->with('usuario');
            }
        ];

        try {
            // Si la solicitud se hizo desde el conductor
            // devolvemos las solicitudes que ha atentido
            // La ruta que se ejecuta es api.v1.conductores.solicitudes.index
            // por lo que comparamos si el nomre de la ruta contiene la palabra conductor
            if (stripos(\Illuminate\Support\Facades\Route::currentRouteName(), 'conductor') !== false) {
                // Si realmente la petición se realizó desde el conductor
                // obtenemos el usuario
                $user = JWTAuth::parseToken()->authenticate();

                if (empty($user)) {
                    return response()->jsonNotFound(['message' => 'Conductor no encontrado']);
                }

                // Agregamos a los filtros el id del conductor
                $params['conductor_id'] = $user->id;
                // Eliminamos el dato del conductor del with, porque no es necesario
                // ya que todas las solicitudes son de un mismo conductor
                unset($with['conductor']);
            }

            // Si la solicitud se hizo desde el pasajero
            // devolvemos todas sus solicitudes
            // La ruta que se ejecuta es api.v1.pasajeros.solicitudes.index
            // por lo que comparamos si el nomre de la ruta contiene la palabra pasajero
            if (stripos(\Illuminate\Support\Facades\Route::currentRouteName(), 'pasajero') !== false) {
                // Si realmente la petición se realizó desde el pasajero
                // obtenemos el usuario
                $user = JWTAuth::parseToken()->authenticate();

                if (empty($user)) {
                    return response()->jsonNotFound(['message' => 'Pasajero no encontrado']);
                }

                // Agregamos a los filtros el id del pasajero
                $params['pasajero_id'] = $user->id;
                // Eliminamos el dato del pasajero del with, porque no es necesario
                // ya que todas las solicitudes son de un mismo pasajero
                unset($with['pasajero']);
            }

            // Si se esta utilizando paginación
            if (isset($params['page'])) {
                // Eliminamos el campo de paginación del listado de parámetros
                unset($params['page']);
                $result = null;

                // Si desea filtrar por alguna otro campo
                // tiene que enviar el nombre del campo y su respectivo valor
                // en la URL como si fuera método GET
                if (count($params)) {
                    $params = array_filter($params);
                    $where = [];
                    foreach ($params as $key => $value) {
                        if ($key == 'fecha_abordaje') {
                            $where[] = [$key, '>=', $value];
                        } else {
                            $where[] = [$key, '=', $value];
                        }
                    }

                    $result = Solicitud::with($with)
                        ->where($where)
                        ->orderBy('created_at', 'desc')
                        ->paginate(6);
                } else {
                    $result = Solicitud::with($with)->paginate(6);
                }

                $paginator = $result->toArray();
                // Obtenemos el listado de registros
                $data = $paginator['data'];
                // Eliminamos los datos del paginador,
                // para enviar solo los metadatos de la paginación
                unset($paginator['data']);
                unset($paginator['next_page_url']);
                unset($paginator['prev_page_url']);
            } else {
                $paginator = null;
                $data = null;

                // Si desea filtrar por alguna otro campo
                // tiene que enviar el nombre del campo y su respectivo valor
                // en la URL como si fuera método GET
                if (count($params)) {
                    $params = array_filter($params);
                    $where = [];
                    foreach ($params as $key => $value) {
                        if ($key == 'fecha_abordaje') {
                            $where[] = [$key, '>=', $value];
                        } else {
                            $where[] = [$key, '=', $value];
                        }
                    }

                    $data = Solicitud::with($with)
                        ->where($where)
                        ->orderBy('created_at', 'desc')
                        ->get();
                } else {
                    $data = Solicitud::with($with)->get();
                }
                //$data->load('concesion', 'aseguradora');
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
            'extra' => $paginator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();

            if (empty($user)) {
                return response()->jsonNotFound(['message' => 'Usuario no disponible']);
            }

            $data = $request->all();
            // Se crea la solicitud con el estado Nuevo
            $data['estado'] = Solicitud::$ESTADO_NUEVO;
            $data['pasajero_id'] = $user->id;

            $model = Solicitud::create($data);

            $municipios_notificar = $model->getMunicipiosNotificar();

            if (empty($municipios_notificar)) {
                DB::rollBack();
                return response()->jsonNotFound(['message' => 'Fuera del area de servicio.']);
            }

            // Se notifica la solicitud a los conductores
            OneSignal::notificarSolicitudConductores([
                'tags' => ['municipio_id' => $municipios_notificar],
                'data' => ['solicitud_id' => $model->id, 'latitud' => $model->latitud, 'longitud' => $model->longitud]
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            if ($e->getCode() == 404) {
                return response()->jsonNotFound(['message' => 'No hay Taxis disponibles para atender su solicitud.']);
            }

            return response()->jsonException($e);
        }

        DB::commit();
        // Cuando guardamos devolvemos el elemento que se creó
        return response()->jsonSuccess([
            'data' => $model->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws 409 - Si el conductor tiene un servicio activo
     *         404 - Usuario no encontrado
     *         403 - El servicio ya ha sido asignado
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $conductor_tomo_servicio = false;

        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            //Solicitud::lockTable();
            $data = $request->all();

            // Campos que no pueden ser actualizados desde el exterior
            if (isset($data['pasajero_id'])) {
                unset($data['pasajero_id']);
            }
            if (isset($data['estado'])) {
                unset($data['estado']);
            }
            if (isset($data['fecha_abordaje'])) {
                unset($data['fecha_abordaje']);
            }
            if (isset($data['fecha_arribo_destino'])) {
                unset($data['fecha_arribo_destino']);
            }
            if (isset($data['conductor_id'])) {
                unset($data['conductor_id']);
            }

            // Obtenemos el usuario que esta realizando la petición
            $user = JWTAuth::parseToken()->authenticate();

            if (empty($user)) {
                //Solicitud::unlockTable();
                return response()->jsonNotFound(['message' => 'Usuario no disponible.']);
            }

            // Si la solicitud se hizo desde el conductor quiere decir
            // que va tomar el servicio.
            // La ruta que se ejecuta es api.v1.conductores.solicitudes.update
            // por lo que comparamos si el nomre de la ruta contiene la palabra conductor
            if (stripos(\Illuminate\Support\Facades\Route::currentRouteName(), 'conductor') !== false) {
                // Primero revisamos si la solicitud no ha asignada cancelada
                if ($this->modelInst->estado == Solicitud::$ESTADO_CANCELADO) {
                    //Solicitud::unlockTable();
                    return response()->jsonForbidden(['message' => 'El Servicio esta cancelado']);
                }

                // Primero revisamos que la solicitud no este asignada
                if ($this->modelInst->conductor_id != null) {
                    //Solicitud::unlockTable();
                    return response()->jsonForbidden(['message' => 'El Servicio ya esta asignado']);
                }

                // Si realmente la petición se realizó desde el conductor
                // asignamos el usuario a la solicitud
                $data['conductor_id'] = $user->id;
                $data['estado'] = Solicitud::$ESTADO_ASIGNADO;
                $conductor_tomo_servicio = true;
            }

            // Validamos que el pasajero que accede a la solicitud sea
            // el que realizó la solicitud
            if ($user->isPasajero()) {
                if ($user->id != $this->modelInst->pasajero_id) {
                    //Solicitud::unlockTable();
                    return response()->jsonForbidden(['message' => 'No puede acceder a la solicitud porque no es el pasajero que realizó la solicitud.']);
                }
            }

            // Validamos que el conductor que accede a la solicitud sea realmente
            // el que tomó la solicitud
            if ($user->isConductor()) {
                if (!empty($this->modelInst->conductor_id) && $user->id != $this->modelInst->conductor_id) {
                    //Solicitud::unlockTable();
                    return response()->jsonForbidden(['message' => 'No puede acceder a la solicitud porque no es el conductor asignado al servicio.']);
                }
            }

            $validator = Validator::make($data, Solicitud::$rules_update, Solicitud::$messages);

            if ($validator->fails()) {
                //Solicitud::unlockTable();
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }

            $this->modelInst->fill($data);
            // Se sobrescribre el metodo save para realizar las validaciones de
            // si el conductor no tiene un servicio activo o
            // si el servicio ya ha sido tomado por otro conductor
            $this->modelInst->checkAndSave($conductor_tomo_servicio);

            if ($conductor_tomo_servicio) {
                OneSignal::notificarSolicitudPasajero([
                    'tags' => ['pasajero_id' => $this->modelInst->pasajero_id],
                    'data' => ['solicitud_id' => $this->modelInst->id, 'tiempo_llegada' => $this->modelInst->tiempo_llegada]
                ]);
                OneSignal::notificarAsignacionServicioConductor([
                    'tags' => ['conductor_id' => $this->modelInst->conductor_id],
                    'data' => ['solicitud_id' => $this->modelInst->id]
                ]);
            }

            $response = $this->modelInst->toArray();

            // Eliminamos datos que no queremos enviar en la respuesta
            unset($response['created_at']);
            unset($response['updated_at']);

            //Solicitud::unlockTable();
            DB::commit();
        } catch (\Exception $e) {
            //Solicitud::unlockTable();
            DB::rollBack();

            if ($e->getCode() == 404) {
                return response()->jsonNotFound(['message' => 'No se pudo notificar al Pasajero que su solicitud ha sido aceptada.']);
            }

            // Cuando el conductor tiene un servicio activo
            // se lanza la excepcion 409 con el id de la solicitud activa
            // por lo que hay que cacharla y obtener la información de la solicitud
            if ($e->getCode() == 409) {
                $solicitud = Solicitud::with([
                        'conductor' => function ($query) {
                            $query->with(['usuario','taxi']);
                        },
                        'pasajero' => function ($query) {
                            $query->with('usuario');
                        }
                    ])->find($e->getMessage());

                return response()->jsonException($e, [
                    'data' => $solicitud,
                    'message' => 'No puede tomar el servicio debido a que tiene un servicio activo.',
                ]);
            }

            return response()->jsonException($e);
        }

        // Cuando actualizamos devolvemos el elemento que se modificó
        return response()->jsonSuccess([
            'data' => $response
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            $user = JWTAuth::parseToken()->authenticate();

            // Validamos que el pasajero que cancela sea realmente
            // el que realizó la solicitud
            if ($user->isPasajero()) {
                if ($user->id != $this->modelInst->pasajero_id) {
                    return response()->jsonForbidden(['message' => 'No puede cancelar la solicitud porque no es el pasajero que realizó la solicitud.']);
                }
            }

            // Validamos que el conductor que cancela sea realmente
            // el que tomó la solicitud
            if ($user->isConductor()) {
                if ($user->id != $this->modelInst->conductor_id) {
                    return response()->jsonForbidden(['message' => 'No puede cancelar la solicitud porque no es el conductor asignado al servicio.']);
                }
            }

            // Las solicitudes no se eliminarán solo se
            // establecerán el estado como cancelado
            $this->modelInst->estado = Solicitud::$ESTADO_CANCELADO;
            $this->modelInst->save();

            if ($user->isConductor()) {
                OneSignal::notificarCancelacionAlPasajero([
                    'tags' => ['pasajero_id' => $this->modelInst->pasajero_id],
                    'data' => ['solicitud_id' => $this->modelInst->id]
                ]);
            } else {
                if( !empty($this->modelInst->conductor_id) ){
                    OneSignal::notificarCancelacionAlConductor([
                        'tags' => ['conductor_id' => $this->modelInst->conductor_id],
                        'data' => ['solicitud_id' => $this->modelInst->id]
                    ]);
                }
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Establece el abordaje del pasajero
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function abordar(Request $request, $id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        $user = JWTAuth::parseToken()->authenticate();

        if ($user->id != $this->modelInst->conductor_id) {
            return response()->jsonForbidden(['message' => 'No puede finalizar el servicio porque no es el conductor asignado al servicio.']);
        }

        try {
            $this->modelInst->estado = Solicitud::$ESTADO_ABORDADO;
            $this->modelInst->fecha_abordaje = date('Y-m-d H:i:s');
            $this->modelInst->save();

            $response = $this->modelInst->toArray();

            OneSignal::notificarPasajeroABordo([
                    'tags' => ['pasajero_id' => $this->modelInst->pasajero_id],
                    'data' => ['solicitud_id' => $this->modelInst->id]
                ]);

            // Eliminamos datos que no queremos enviar en la respuesta
            unset($response['created_at']);
            unset($response['updated_at']);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $response
        ]);
    }

    /**
     * Establece la finalización del servicio
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finalizar($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        $user = JWTAuth::parseToken()->authenticate();

        if ($user->id != $this->modelInst->conductor_id) {
            return response()->jsonForbidden(['message' => 'No puede finalizar el servicio porque no es el conductor asignado.']);
        }

        try {
            $this->modelInst->estado = Solicitud::$ESTADO_TERMINADO;
            $this->modelInst->fecha_arribo_destino = date('Y-m-d H:i:s');
            $this->modelInst->save();

            OneSignal::notificarFinServicio([
                        'tags' => ['pasajero_id' => $this->modelInst->pasajero_id],
                        'data' => ['solicitud_id' => $this->modelInst->id]
                    ]);

            $response = $this->modelInst->toArray();

            // Eliminamos datos que no queremos enviar en la respuesta
            unset($response['created_at']);
            unset($response['updated_at']);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $response
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function modopanico(Request $request, $id)
    {
        $data = Solicitud::enModoPanicoFromPasajero($id);

        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Devuelve archivo csv con el reporte de solicitudes
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reporte(Request $request)
    {
        $reporte = [];

        try {
            $reporte = Solicitud::reporte(array_filter($request->all()));

            // Se agrega el nombre de las columnas
            array_unshift($reporte, [
                'Organización',
                'Concesionaria',
                'No. Económico',
                'Placas',
                'Conductor',
                'Servicios Atendidos',
                'Servicios Cancelados',
            ]);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        /*return response()->jsonSuccess([
            'data' => Solicitud::arrayToCsv($reporte)
        ]);*/

        return response(Solicitud::arrayToCsv($reporte), 200)
            ->header('Content-Type', 'application/csv; charset=UTF-8')
            ->header('Content-Disposition', 'attachment; filename="Reporte_'.date('d-m-Y_H-i-s').'.csv";')
            ->header('Pragma', 'no-cache')
            ->header('charset', 'utf-8')
            ->header('Expires', '0');
    }
}
