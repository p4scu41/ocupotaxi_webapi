<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Municipio;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = Municipio::all();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
        ]);
    }
}
