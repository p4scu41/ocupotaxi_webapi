<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use App\Models\ServidorTracking;

class AuthJwtController extends Controller
{
    /**
     * Autentifica al usuario y devuelve el token generado
     *  
     * @param  Request $request
     * @return Response JSON Token
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // Solo prodrán loguearse usuarios activos
        $credentials['activo'] = 1;
        $data = ['token'=> null, 'token_tracking' => null];

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                // Utilizamos la clase personalizada para devolver la respuesta
                return response()->jsonFormat([
                    'error' => 'invalid_credentials',
                    'message' => 'Usuario o Contraseña incorrecta'
                ], 409);
            }

            // Obtenemos el usuario que se logueo
            $user = JWTAuth::authenticate($token);
            
            $data['usuario_id'] = $user->id;
            $data['token'] = $token;
            

            // Desde la app web solo se pueden loguear usuarios administradores
            // por lo tanto se válida si la solicitud se hizo desde la app web
            // el usuario debe ser administrador
            if ($request->input('appweb') && !$user->isAdministrador()) {
                return response()->jsonForbidden([
                    'data' => 'Usuario no autorizado'
                ]);
            }

            // Si el usuario que se logueó es un conductor o administrador
            // tiene permitido hacer peticiones al servidor de tracking
            // por lo que será necesario obtener el token tracking
            //if ($user->isConductor() || $user->isAdministrador()) {
                // Obtenemos el Token para Tracking
                $token_tracking = ServidorTracking::obtenerToken([
                    'tipo' => $user->rol_id,
                    'conductor_id' => $user->id,
                ]);

                $user->token_tracking = $token_tracking;
                $user->save();

                $data['token_tracking'] = $user->token_tracking;
            //}
        } catch (JWTException $e) {
            // something went wrong
            // Utilizamos la clase personalizada para devolver la respuesta
            return response()->jsonFormat([
                'error' => 'could_not_create_token',
                'message' => 'Error al iniciar sesión. Intentelo nuevamente.',
                'extra' => $e->getMessage()
            ], 500);
        } catch (\Exception $e) {
            // Solamente se envía la notificación del error cuando 
            // no se pueda obtener el token JWT
            // porque puede que no se pueda obtener el token tracking
            // pero este solo es necesario para visualizar el mapa
            if (empty($data['token'])) {
                return response()->jsonException($e);
            }
        }

        // if no errors are encountered we can return a JWT
        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Genera un nuevo token en caso de que este expire
     * 
     * @param  Request $request 
     * @return Response JSON Token
     */
    public function refresh(Request $request)
    {
        try {
            $newToken = JWTAuth::setRequest($request)->parseToken()->refresh();

            // Obtenemos el usuario que se logueo
            $user = JWTAuth::authenticate($newToken);
            $data['token'] = $newToken;

            // Si el usuario que se logueó es un conductor o administrador
            // tiene permitido hacer peticiones al servidor de tracking
            // por lo que será necesario obtener el token tracking
            if ($user->isConductor() || $user->isAdministrador()) {
                // Obtenemos el Token para Tracking
                $token_tracking = ServidorTracking::obtenerToken([
                    'tipo' => $user->rol_id,
                    'conductor_id' => $user->id,
                ]);

                $user->token_tracking = $token_tracking;
                $user->save();

                $data['token_tracking'] = $user->token_tracking;
            }
        } catch (TokenExpiredException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token expirado']);
        } catch (TokenInvalidException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token inválido']);
        } catch (JWTException $e) {
            return response()->jsonJwtException($e, ['message' => 'Error al procesar el Token']);
        }

        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Obtiene datos del usuario a partir del token recibido
     * 
     * @return Array User
     */
    public function user()
    {
        $user = null;

        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->jsonNotFound([
                    'message' => 'Usuario no encontrado',
                    'extra' => 'Not Found',
                ]);
            }

            // Carga los datos de pasajero o conductor
            // dependiendo del tipo de usuario
            if ($user->isConductor()) {
                $user->load('conductor');

                // Cargamos los datos del taxi, asociado con el conductor
                if (!empty($user->conductor)) {
                    $user->conductor->load('taxi');
                }
            }

            if ($user->isPasajero()) {
                $user->load('pasajero');
            }
        } catch (TokenExpiredException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token expirado']);
        } catch (TokenInvalidException $e) {
            return response()->jsonJwtException($e, ['message' => 'Token inválido']);
        } catch (JWTException $e) {
            return response()->jsonJwtException($e, ['message' => 'Error al procesar el Token']);
        }

        $data = $user->toArray();
        //$data['foto'] = $user->parseFoto();
        $data['foto'] = $user->getUrlFoto();

        return response()->jsonSuccess([
            'data' => $data
        ]);
    }
}
