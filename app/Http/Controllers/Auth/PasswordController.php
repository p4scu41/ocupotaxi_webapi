<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    public $subject = 'Ocupotaxi: Cambiar contraseña';

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // Las siguientes funciones se sobreescriben de 
    // Illuminate\Foundation\Auth\ResetsPasswords

    /**
     * Se sobreescribe esta funcion para devolver una respuesta JSON
     * 
     * @param  string $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->jsonSuccess([
            'message' => 'Se ha enviado un mensaje a la dirección de correo electrónico proporcionado con las instrucciones para recuperar su contraseña.'
        ]);
    }

    /**
     * Se sobreescribe esta funcion para devolver una respuesta JSON
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailFailureResponse($response)
    {
        // trans -> Utiliza el sistema de lenguajes para traducir el mensaje de error devuelto
        return response()->jsonNotFound([
            'message' => trans($response)
        ]);
    }

    /**
     * Se sobreescribe esta función para devolver una página personalizada
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        // Después de actualizar la contraseña
        // inicia sesión de forma automática
        // por lo que es necesario cerrar la sesión
        Auth::logout();

        return redirect('password/resetsuccess');
    }
}
