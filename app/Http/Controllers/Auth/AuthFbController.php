<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use App\Models\User;
use App\Models\Pasajero;
use App\Models\ServidorTracking;

class AuthFbController extends Controller
{
    /**
     * Autentifica al usuario y devuelve el token generado
     *  
     * @param  Request $request
     * @return Response JSON Token
     */
    public function authenticate(Request $request)
    {
        $data = $request->all();

        if (!isset($data['facebook_id'])) {
            return response()->jsonInvalidData(['message' => 'facebook_id es obligatorio']);
        }

        try {
            // Primer buscamos el pasajero por su facebook_id 
            $pasajero = Pasajero::getByFb($data['facebook_id']);

            //return response()->json($pasajero,200);

            // Si no se encuentra el pasajero por su facebook_id
            if (count($pasajero) == 0) {
                //return response()->json('pasajero vacio',200);
                // Procedemos a registrarlo
                $pasajero = Pasajero::registerByFb($data);
            }

            //return response()->json($pasajero,200);

            // Obtenemos el usuario del pasajero
            $user = User::find($pasajero['usuario_id']);
            
            if (isset($data['avatar'])) {
                $user->avatar = $data['avatar'];
                $user->save();
            }

            // Obtenemos el token a partir del modelo del usuario
            if (! $token = JWTAuth::fromUser($user)) {
                // Utilizamos la clase personalizada para devolver la respuesta
                return response()->jsonFormat([
                    'error' => 'invalid_credentials',
                    'message' => 'Usuario no disponible'
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            // Utilizamos la clase personalizada para devolver la respuesta
            return response()->jsonFormat([
                'error' => 'could_not_create_token',
                'message' => 'Error al iniciar sesión. Intentelo nuevamente.',
                'extra' => $e->getMessage()
            ], 500);
        } catch (\Exception $e) {
            // something went wrong
            // Utilizamos la clase personalizada para devolver la respuesta
            return response()->jsonFormat([
                'error' => 'could_not_create_token',
                'message' => 'Error al iniciar sesión. Intentelo nuevamente.',
                'extra' => $e->getMessage()
            ], 500);
        }
        
        $token_tracking = ServidorTracking::obtenerToken([
                    'tipo' => $user->rol_id,
                    'conductor_id' => $user->id,
                ]);

        $user->token_tracking = $token_tracking;
        $user->save();

        // if no errors are encountered we can return a JWT
        return response()->jsonSuccess([
            'data' => ['usuario_id' => $user->id,'token' => $token,'token_tracking' => $token_tracking]
        ]);
    }
}
