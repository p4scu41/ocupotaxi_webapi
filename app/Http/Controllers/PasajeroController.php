<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Pasajero;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use Validator;
use JWTAuth;
use DB;

class PasajeroController extends Controller
{
    // Guarda la entidad que se solicita cuando se ejecuta las rutas:
    // show, edit, update, destroy
    public $modelInst = null;

    // Nombre del recurso que se expone en las rutas
    public $resource = 'pasajeros';

    function __construct(Route $route)
    {
        $this->find($route);
    }

    /**
     * Get the resource with the specific ID
     * Se ejecuta cuando se solicitan las rutas:
     * show, edit, update, destroy
     *
     * @param  Route  $route
     * @return void   Redirect when the resource is not found
     */
    public function find(Route $route)
    {
        try {
            // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
            $id = $route->getParameter($this->resource) ? $route->getParameter($this->resource) : $route->getParameter('id');
            $this->modelInst = Pasajero::with(['usuario' => function ($query) {
                $query->with('municipio');
            }])->find($id);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->input();

        try {
            // Si se esta utilizando paginación
            if (isset($params['page'])) {
                // Eliminamos el campo de paginación del listado de parámetros
                unset($params['page']);
                $result = null;

                // Si desea filtrar por alguna otro campo
                // tiene que enviar el nombre del campo y su respectivo valor
                // en la URL como si fuera método GET
                if (count($params)) {
                    // Limpiamos el arreglo de cualquier valor vacío
                    $params = array_filter($params);
                    $where = [];
                    // Se construye un array de where
                    // con el comparativo like y
                    foreach ($params as $key => $value) {
                        $where[] = [$key, 'like', '%'.$value.'%', 'OR'];
                    }

                    $result = User::where('rol_id', '=', User::$ROL_PASAJERO)
                        ->with('municipio', 'pasajero')
                        ->where($where)
                        ->paginate();
                } else {
                    $result = User::where('rol_id', '=', User::$ROL_PASAJERO)
                        ->with('municipio', 'pasajero')
                        ->paginate();
                }

                $paginator = $result->toArray();

                // Obtenemos el listado de registros
                $data = $paginator['data'];
                // Eliminamos los datos del paginador,
                // para enviar solo los metadatos de la paginación
                unset($paginator['data']);
                unset($paginator['next_page_url']);
                unset($paginator['prev_page_url']);
            } else {
                $paginator = null;
                $data = null;

                // Si desea filtrar por alguna otro campo
                // tiene que enviar el nombre del campo y su respectivo valor
                // en la URL como si fuera método GET
                if (count($params)) {
                    $params = array_filter($params);
                    $where = [];
                    foreach ($params as $key => $value) {
                        $where[] = [$key, 'like', '%'.$value.'%', 'OR'];
                    }

                    $data = User::where('rol_id', '=', User::$ROL_PASAJERO)
                        ->with('municipio', 'pasajero')
                        ->where($where)->get();
                } else {
                    $data = User::where('rol_id', '=', User::$ROL_PASAJERO)
                        ->with('municipio', 'pasajero')->get();
                }
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
            'extra' => $paginator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $user_data = $request->all();

            // Establecemos el rol como pasajero
            $user_data['rol_id'] = User::$ROL_PASAJERO;
            // Por defecto, el pasajero se registra como activo
            $user_data['activo'] = 1;

            // Validamos los datos del usuario
            $validator = Validator::make($user_data, User::$rules, User::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }
             // Encriptamos la contraseña
            $user_data['password'] = \Hash::make($user_data['password']);
            $user = User::create($user_data);

            $pasajero_data = $request->all();
            $pasajero_data['usuario_id'] = $user->id;

            // Validamos los datos del pasajero
            $validator = Validator::make($pasajero_data, Pasajero::$rules, Pasajero::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }

            Pasajero::create($pasajero_data);
            // Por alguna extraña razón no devuelve los datos insertados en la tabla
            // por lo que se procede a obtener el registro del pasajero
            $pasajero = Pasajero::find($user->id);
            $pasajero->load('usuario');

            // Para subir la foto
            $user->uploadFoto($request);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        DB::commit();
        // Cuando guardamos devolvemos el elemento que se creó
        return response()->jsonSuccess([
            'data' => $pasajero->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        $data = $this->modelInst->toArray();
        // Obtenemos la foto como cadena en base64
        //$data['foto'] = $this->modelInst->usuario->parseFoto();
        $data['foto'] = $this->modelInst->usuario->getUrlFoto();

        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     * Send the data with the header Content-Type: application/x-www-form-urlencoded
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            DB::beginTransaction();

            $user_data = $request->all();

            // Evitamos que cambien el ID del usuario
            unset($user_data['id']);

            // Establecemos el rol como pasajero
            $user_data['rol_id'] = User::$ROL_PASAJERO;

            // Validamos los datos del usuario
            $validator = Validator::make($user_data, User::$rules_update, User::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }

            // Si envia password, lo encriptamos la contraseña
            if (isset($user_data['password'])) {
                $user_data['password'] = \Hash::make($user_data['password']);
            }

            $user = User::find($id);
            $user->fill($user_data);
            $user->save();

            $this->modelInst->usuario()->associate($user);

            $pasajero_data = $request->all();
            // Evitamos que cambien el ID del pasajero
            unset($pasajero_data['usuario_id']);

            // Validamos los datos del pasajero
            $validator = Validator::make($pasajero_data, Pasajero::$rules_update, Pasajero::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }

            $this->modelInst->fill($pasajero_data);
            $this->modelInst->save();

            // Para subir la foto
            $user->uploadFoto($request);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        DB::commit();
        // Cuando actualizamos devolvemos el elemento que se modificó
        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            DB::beginTransaction();
            $data = $this->modelInst->toArray();
            $this->modelInst->delete();

            // Debemos eliminar el registro en usuarios
            // ya que es del que depende este registro
            $user = User::find($id);
            $user->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        DB::commit();
        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Ejecuta la actualización desde el cliente móvil
     *
     * @param  Request $request
     * @return Response
     */
    public function actualizar(Request $request)
    {
        // Se obtiene el usuario a partir del token
        // no es necesario enviar el ID del pasajero para actualizarlo
        $user = JWTAuth::parseToken()->authenticate();

        if (empty($user)) {
            return response()->jsonNotFound();
        }

        // Se obtiene el Pasajero a partir del usuario
        $this->modelInst = $user->pasajero;

        // Se ejecuta la acción update de forma normal,
        // como si la petición se hiciera directamente a la ruta
        return $this->update($request, $this->modelInst->usuario_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function modopanico(Request $request)
    {
        $data = Pasajero::enModoPanico();

        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Sube la foto del pasajero
     *
     * @param  Request $request
     * @return Response
     */
    public function uploadfoto(Request $request)
    {
        // Se obtiene el usuario a partir del token
        // no es necesario enviar el ID del pasajero para actualizarlo
        $user = JWTAuth::parseToken()->authenticate();

        if (empty($user)) {
            return response()->jsonNotFound();
        }

        try {
            $message = 'Imagen subida exitosamente';

            if (!$user->uploadFoto($request)) {
                $message = 'Imagen no subida';
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'message' => $message,
        ]);
    }
}
