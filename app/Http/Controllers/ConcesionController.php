<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ConcesionRequest;
use App\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\Concesion;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use Validator;

class ConcesionController extends Controller
{
    // Guarda la entidad que se solicita cuando se ejecuta las rutas:
    // show, edit, update, destroy
    public $modelInst = null;

    // Nombre del recurso que se expone en las rutas
    public $resource = 'concesiones';

    function __construct(Route $route)
    {
        $this->find($route);
    }

    /**
     * Get the resource with the specific ID
     * Se ejecuta cuando se solicitan las rutas:
     * show, edit, update, destroy
     * 
     * @param  Route  $route 
     * @return void   Redirect when the resource is not found
     */
    public function find(Route $route)
    {
        try {
            // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
            $id = $route->getParameter($this->resource) ? $route->getParameter($this->resource) : $route->getParameter('id');
            $this->modelInst = Concesion::with('municipio')->find($id);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->input('page');

        try {
            // Si se esta utilizando paginación
            if ($page) {
                $paginator = Concesion::with('municipio')->orderBy('nombre_concesionado')->paginate()->toArray();
                // Obtenemos el listado de registros
                $data = $paginator['data'];
                // Eliminamos los datos del paginador, 
                // para enviar solo los metadatos de la paginación
                unset($paginator['data']);
                unset($paginator['next_page_url']);
                unset($paginator['prev_page_url']);
            } else {
                $paginator = null;
                $data = Concesion::orderBy('nombre_concesionado')->get();
                $data->load('municipio');
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
            'extra' => $paginator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConcesionRequest $request)
    {
        try {
            $model = Concesion::create($request->all());
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando guardamos devolvemos el elemento que se creó
        return response()->jsonSuccess([
            'data' => $model->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            $validator = Validator::make($request->all(), Concesion::$rules_update, Concesion::$messages);

            if (!$validator->fails()) {
                $this->modelInst->fill($request->all());
                $this->modelInst->save();
            } else {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando actualizamos devolvemos el elemento que se modificó
        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }
            
        try {
            $data = $this->modelInst->toArray();
            $this->modelInst->delete();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $data
        ]);
    }
}
