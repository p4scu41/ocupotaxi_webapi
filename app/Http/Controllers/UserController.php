<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use JWTAuth;

class UserController extends Controller
{
    // Guarda la entidad que se solicita cuando se ejecuta las rutas:
    // show, edit, update, destroy
    public $user = null;

    function __construct(Route $route)
    {
        $this->find($route);
    }

    /**
     * Get de user with the specific ID
     * Se ejecuta cuando se solicitan las rutas:
     * show, edit, update, destroy
     *
     * @param  Route  $route
     * @return void   Redirect when the resource is not found
     */
    public function find(Route $route)
    {
        try {
            // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
            $id = $route->getParameter('usuarios') ? $route->getParameter('usuarios') : $route->getParameter('id');
            $this->user = User::where('id', '=', $id)->first();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->input('page');
        $user = JWTAuth::parseToken()->authenticate();

        try {
            // Si se esta utilizando paginación
            if ($page) {
                $paginator = User::where('rol_id', '=', User::$ROL_ADMINISTRADOR)
                    //->where('id', '!=', $user->id) // No devolvemos los datos del usuario logueado en el list
                    ->paginate()->toArray();
                // Obtenemos el listado de usuarios
                $users = $paginator['data'];
                // Eliminamos los datos del paginador,
                // para enviar solo los metadatos de la paginación
                unset($paginator['data']);
                unset($paginator['next_page_url']);
                unset($paginator['prev_page_url']);
            } else {
                $paginator = null;
                $users = User::where('rol_id', '=', User::$ROL_ADMINISTRADOR)
                    ->where('id', '!=', $user->id) // No devolvemos los datos del usuario logueado en el list
                    ->get();
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $users,
            'extra' => $paginator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try {
            $data = $request->all();
            // encriptamos la contraseña
            $data['password'] = \Hash::make($data['password']);

            $user = User::create($data);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando guardamos devolvemos el elemento que se creó
        return response()->jsonSuccess([
            'data' => $user->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (empty($this->user)) {
            return response()->jsonNotFound();
        }

        return response()->jsonSuccess([
            'data' => $this->user->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($this->user)) {
            return response()->jsonNotFound();
        }

        try {
            $data = $request->all();

            if (isset($data['password'])) {
                // Si tambien esta actualizando el password
                // encriptamos la contraseña
                $data['password'] = \Hash::make($data['password']);
            }

            $this->user->fill($data);
            $this->user->save();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando actualizamos devolvemos el elemento que se modificó
        return response()->jsonSuccess([
            'data' => $this->user->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($this->user)) {
            return response()->jsonNotFound();
        }

        try {
            $data = $this->user->toArray();
            $this->user->delete();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Obtiene la url de la foto
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function foto($id)
    {
        try {
            // Devolvemos la foto como archivo
            return response()->file($this->user->getPathFoto());
        } catch (\Exception $e) {
            return response()->jsonNotFound();
        }
    }

    /**
     * Devuelve el archivo de terminos y condiciones de uso
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function terminos()
    {
        try {
            // Devolvemos la foto como archivo
            return response()->download(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'terminos.rtf');
        } catch (\Exception $e) {
            return response()->jsonNotFound();
        }
    }
}
