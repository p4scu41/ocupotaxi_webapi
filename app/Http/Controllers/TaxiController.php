<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaxiRequest;
use App\Http\Controllers\Controller;
use App\Models\Aseguradora;
use App\Models\Concesion;
use App\Models\Modalidad;
use App\Models\Taxi;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use Validator;

class TaxiController extends Controller
{
    // Guarda la entidad que se solicita cuando se ejecuta las rutas:
    // show, edit, update, destroy
    public $modelInst = null;

    // Nombre del recurso que se expone en las rutas
    public $resource = 'taxis';

    function __construct(Route $route)
    {
        $this->find($route);
    }

    /**
     * Get the resource with the specific ID
     * Se ejecuta cuando se solicitan las rutas:
     * show, edit, update, destroy
     * 
     * @param  Route  $route 
     * @return void   Redirect when the resource is not found
     */
    public function find(Route $route)
    {
        try {
            // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
            $id = $route->getParameter($this->resource) ? $route->getParameter($this->resource) : $route->getParameter('id');
            $this->modelInst = Taxi::find($id);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->input();

        try {
            // Si se esta utilizando paginación
            if (isset($params['page'])) {
                // Eliminamos el campo de paginación del listado de parámetros
                unset($params['page']);
                // Limpiamos el arreglo de cualquier valor vacío
                $params = array_filter($params);
                $result = null;

                // Si desea filtrar por alguna otro campo
                // tiene que enviar el nombre del campo y su respectivo valor
                // en la URL como si fuera método GET
                if (count($params)) {
                    $result = Taxi::with('concesion', 'modalidad')->where($params)->orderBy('numero_economico')->paginate();
                } else {
                    $result = Taxi::with('concesion', 'modalidad')->orderBy('numero_economico')->paginate();
                }

                $paginator = $result->toArray();
                // Obtenemos el listado de registros
                $data = $paginator['data'];
                // Eliminamos los datos del paginador, 
                // para enviar solo los metadatos de la paginación
                unset($paginator['data']);
                unset($paginator['next_page_url']);
                unset($paginator['prev_page_url']);
            } else {
                $paginator = null;
                $data = null;
                $params = array_filter($params);

                // Si desea filtrar por alguna otro campo
                // tiene que enviar el nombre del campo y su respectivo valor
                // en la URL como si fuera método GET
                if (count($params)) {
                    $data = Taxi::where($params)->orderBy('numero_economico')->get();
                } else {
                    $data = Taxi::orderBy('numero_economico')->get();
                }
                $data->load('concesion', 'modalidad');
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
            'extra' => $paginator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaxiRequest $request)
    {
        try {
            $model = Taxi::create($request->all());
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando guardamos devolvemos el elemento que se creó
        return response()->jsonSuccess([
            'data' => $model->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            $validator = Validator::make($request->all(), Taxi::$rules_update, Taxi::$messages);

            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }
            
            $this->modelInst->fill($request->all());
            $this->modelInst->save();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando actualizamos devolvemos el elemento que se modificó
        return response()->jsonSuccess([
            'data' => $this->modelInst->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }
            
        try {
            $data = $this->modelInst->toArray();
            $this->modelInst->delete();
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Devuelve todos los catalogos de los que depende Taxi
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function catalogos()
    {
        $catalogos = [
            'concesionario' => Concesion::orderBy('nombre_concesionado')->get(),
            'modalidad' => Modalidad::all(),
            'aseguradora' => Aseguradora::all(),
        ];

        return response()->jsonSuccess([
            'data' => $catalogos
        ]);
    }
}
