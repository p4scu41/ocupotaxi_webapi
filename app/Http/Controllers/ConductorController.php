<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Conductor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use Validator;
use JWTAuth;
use DB;

class ConductorController extends Controller
{
    // Guarda la entidad que se solicita cuando se ejecuta las rutas:
    // show, edit, update, destroy
    public $modelInst = null;

    // Nombre del recurso que se expone en las rutas
    public $resource = 'conductores';

    function __construct(Route $route)
    {
        $this->find($route);
    }

    /**
     * Get the resource with the specific ID
     * Se ejecuta cuando se solicitan las rutas:
     * show, edit, update, destroy
     * 
     * @param  Route  $route 
     * @return void   Redirect when the resource is not found
     */
    public function find(Route $route)
    {
        try {
            // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
            $id = $route->getParameter($this->resource) ? $route->getParameter($this->resource) : $route->getParameter('id');
            $this->modelInst = Conductor::with('usuario', 'taxi')->find($id);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->input('page');

        try {
            // Si se esta utilizando paginación
            if ($page) {
                $paginator = Conductor::with('usuario', 'taxi')->paginate()->toArray();
                // Obtenemos el listado de registros
                $data = $paginator['data'];
                // Eliminamos los datos del paginador, 
                // para enviar solo los metadatos de la paginación
                unset($paginator['data']);
                unset($paginator['next_page_url']);
                unset($paginator['prev_page_url']);
            } else {
                $paginator = null;
                $data = Conductor::all();
                $data->load('usuario','taxi');
            }
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'data' => $data,
            'extra' => $paginator,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $user_data = $request->input('usuario');

            // Establecemos el rol como Conductor
            $user_data['rol_id'] = User::$ROL_CONDUCTOR;
            // Por defecto, el Conductor se registra como activo
            $user_data['activo'] = 1;

            // Validamos los datos del usuario
            $validator = Validator::make($user_data, User::$rules, User::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }
             // Encriptamos la contraseña
            $user_data['password'] = \Hash::make($user_data['password']);
            $user = User::create($user_data);

            $conductor_data = $request->all();
            $conductor_data['usuario_id'] = $user->id;

            // Validamos los datos del Conductor
            $validator = Validator::make($conductor_data, Conductor::$rules_store, Conductor::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }

            $model = Conductor::create($conductor_data);
            // Por alguna extraña razón no devuelve el ID de usuario en el modelo Conductor
            // pero si lo inserta en la BD
            $model->usuario_id = $user->id;

            // Para subir la foto
            $user->uploadFoto($request);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        DB::commit();
        // Cuando guardamos devolvemos el elemento que se creó
        return response()->jsonSuccess([
            'data' => $model->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        $data = $this->modelInst->toArray();
        // Obtenemos la foto como cadena en base64
        //$data['foto'] = $this->modelInst->usuario->parseFoto();
        $data['foto'] = $this->modelInst->usuario->getUrlFoto();

        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     * Send the data with the header Content-Type: application/x-www-form-urlencoded
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $modoservicio=false)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            DB::beginTransaction();

            $user_data = $request->input('usuario');

            // Evitamos que cambien el ID del usuario
            unset($user_data['id']);

            // Establecemos el rol como Conductor
            $user_data['rol_id'] = User::$ROL_CONDUCTOR;

            // Validamos los datos del usuario
            $validator = Validator::make($user_data, User::$rules_update, User::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }
            
            // Si envia password, encriptamos la contraseña
            if (isset($user_data['password'])) {
                $user_data['password'] = \Hash::make($user_data['password']);
            }

            $user = $this->modelInst->usuario;
            $user->fill($user_data);
            $user->save();
            
            $this->modelInst->usuario()->associate($user);

            $conductor_data = $request->all();
            // Evitamos que cambien el ID del Conductor
            unset($conductor_data['usuario_id']);

            // Validamos los datos del Conductor
            $validator = Validator::make($conductor_data, Conductor::$rules_update, Conductor::$messages);
            if ($validator->fails()) {
                return response()->jsonInvalidData(['message' => $validator->errors()]);
            }

            $this->modelInst->fill($conductor_data);
            $this->modelInst->save();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        $response = null;

        if ($modoservicio) {
            $response = ['en_servicio' => $this->modelInst->en_servicio];
        } else {
            $response = $this->modelInst->toArray();
        }

        DB::commit();
        // Cuando actualizamos devolvemos el elemento que se modificó
        return response()->jsonSuccess([
            'data' => $response
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }
            
        try {
            DB::beginTransaction();
            $data = $this->modelInst->toArray();
            $this->modelInst->delete();
            
            // Debemos eliminar el registro en usuarios
            // ya que es del que depende este registro
            $user = User::find($id);
            if (!empty($user)) {
                $user->delete();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->jsonException($e);
        }

        DB::commit();
        // Cuando eliminamos devolvemos el elemento que se eliminó
        return response()->jsonSuccess([
            'data' => $data
        ]);
    }

    /**
     * Establece el modo en servicio
     * 
     * @param  Request $request
     * @return Response
     */
    public function modoservicio(Request $request)
    {
        // Se obtiene el usuario a partir del token
        // no es necesario enviar el ID del conductor para establecer el modo en servicio
        $user = JWTAuth::parseToken()->authenticate();

        if (empty($user)) {
            return response()->jsonNotFound();
        }

        // Se obtiene el Conductor a partir del usuario
        $this->modelInst = $user->conductor;

        // Se ejecuta la acción update de forma normal, 
        // como si la petición se hiciera directamente a la ruta
        return $this->update($request, $this->modelInst->usuario_id, true);
    }

    /**
     * Sube la foto del conductor
     * 
     * @param  Request $request
     * @return Response
     */
    public function uploadfoto(Request $request, $id)
    {
        if (empty($this->modelInst)) {
            return response()->jsonNotFound();
        }

        try {
            $this->modelInst->usuario->uploadFoto($request);
        } catch (\Exception $e) {
            return response()->jsonException($e);
        }

        return response()->jsonSuccess([
            'message' => 'Imagen subida exitosamente',
        ]);
    }
}
