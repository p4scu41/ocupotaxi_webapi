<?php
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //Route::auth();
    //Route::get('/home', 'HomeController@index');

    // Ruta para mostrar el formulario para cambiar la contraseña a partir del token
    Route::get('password/reset/{token}', 'Auth\PasswordController@showResetForm');
    // Ruta para ejecutar el cambio de contraseña
    Route::post('password/reset', 'Auth\PasswordController@reset');
    // Ruta para mostrar que cambio de contraseña fue exitoso
    Route::get('password/resetsuccess', function () {
        return view('auth.passwords.resetsuccess');
    });
});

// Se agrega el soporte para CORS a traves del paquete barryvdh/laravel-cors
Route::group(['prefix' => 'api', 'middleware' => ['cors']], function ()
{
    Route::group(['prefix' => 'v1'], function ()
    {
        // Implementacion para jwt-auth -> JSON Web Token Authentication
        // Ejecuta la autenticación con las credenciales del usuario
        Route::post('authjwt', 'Auth\AuthJwtController@authenticate');
        // Devuelve un nuevo token en caso de haber expirado
        Route::delete('refreshjwt', 'Auth\AuthJwtController@refresh');
        // Devuelve los datos del usuario a partir del token
        Route::get('userjwt', 'Auth\AuthJwtController@user');
        // La ruta para registrar pasajero esta libre
        Route::post('pasajeros/registrar', 'PasajeroController@store');
        // La ruta para registrar por Facebook esta libre
        Route::post('pasajeros/authfb', 'Auth\AuthFbController@authenticate');
        // Para enviar enlace de actualización de contraseña
        Route::post('password/email', 'Auth\PasswordController@postEmail');
        // Devuelve la foto del usuario
        Route::get('usuarios/foto/{id}', 'UserController@foto');
        // Descarga el archivo de terminos y condiciones de uso
        Route::get('usuarios/terminos', 'UserController@terminos');
        // Descarga el archivo de terminos y condiciones de uso en html
        //Route::get('usuarios/terminos_html', 'UserController@terminos_html');

        /************************** Administrador *************************************/
        Route::group(['prefix' => 'admin', 'middleware' => ['jwt.auth', 'rol:'.User::$ROL_ADMINISTRADOR]], function () {
            Route::get('municipios', 'MunicipioController@index');
            Route::get('pasajeros/{id}/solicitudespanico', 'SolicitudController@modopanico');
            Route::get('pasajeros/modopanico', 'PasajeroController@modopanico');
            Route::get('taxis/catalogos', 'TaxiController@catalogos');
            Route::resource('usuarios', 'UserController');
            Route::resource('concesiones', 'ConcesionController');
            Route::resource('taxis', 'TaxiController');
            Route::resource('pasajeros', 'PasajeroController');
            Route::resource('conductores', 'ConductorController');
            Route::get('solicitudes/reporte', 'SolicitudController@reporte');
            Route::resource('solicitudes', 'SolicitudController', ['only' => ['index', 'show']]);
            Route::post('conductores/{id}/uploadfoto', 'ConductorController@uploadfoto');
        });
        /*******************************************************************************/

        /************************** Pasajero *************************************/
        Route::group(['prefix' => 'pasajeros', 'middleware' => ['jwt.auth', 'rol:'.User::$ROL_PASAJERO]], function () {
            Route::put('/actualizar', 'PasajeroController@actualizar');
            Route::post('/actualizar', 'PasajeroController@actualizar'); //Fix para que dispositivos Android puedan actualizar
            Route::resource('solicitudes', 'SolicitudController');
            Route::post('solicitudes/{id}/panico', 'AlertaPanicoController@store');
            Route::post('/uploadfoto', 'PasajeroController@uploadfoto');
        });
        /*******************************************************************************/

        /************************** Conductor *************************************/
        Route::group(['prefix' => 'conductores', 'middleware' => ['jwt.auth', 'rol:'.User::$ROL_CONDUCTOR]], function () {
            Route::resource('solicitudes', 'SolicitudController', ['only' => ['index', 'update', 'show', 'destroy']]);
            Route::put('solicitudes/{id}/abordar', 'SolicitudController@abordar');
            Route::put('solicitudes/{id}/finalizar', 'SolicitudController@finalizar');
            Route::put('/modoservicio', 'ConductorController@modoservicio');
        });
        /*******************************************************************************/

    });
});
