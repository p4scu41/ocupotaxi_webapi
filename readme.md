# Documentación de Servicios REST

## Respuesta
Todas las respuestas se devolverán en un array asociativo con el siguiente formato:

    [
        'extra',   // Datos extras que se requieran para procesar la respuesta
        'data',    // Estos son los datos solicitados, cuerpo principal de la respuesta, en la descripción de los servicios se hace referencia como Respuesta
        'message', // Mensaje breve descriptivo de la respuesta, en caso de existir algún error, este puede ser la descripción del error o un arreglo devuelto por el método validate
        'status',  // Código de la respuesta HTTP
        'error',   // Código del error, en la mayoría de los casos es el mismo valor que status
    ];

### Códigos de respuesta HTTP

    200 -> OK, la solicitud se procesó exitosamente
    400 -> Token inválido, el token enviado no es válido, es muy diferente a token expirado
    401 -> Token Expirado, necesita ejecutarse el refresh token
    403 -> Acceso denegado, no se puede ejecutar la petición que se solicita ya que no tiene permisos de acceso
    404 -> Recurso no encontrado, cuando no encuentra alguno recurso solicitado
    422 -> Error de validación de datos, cuando se envía los datos para guardar/actualizar se validan, en caso de existir algún error, este se devuelve con el arreglo de mensajes de validación
    500 -> Error al procesar la solicitud, este se lanza cuando encuentra alguna excepción al procesar la solicitud


## Servicios

Respuesta hace referencia al contenido de la clave data del arreglo devuelto como respuesta.

Los parámetros que aparecen entre corchetes en la URL deben ser sustituidos por su valor correspondiente, por ejemplo [id].

El método PUT debe enviarse con Content-Type: application/x-www-form-urlencoded.

NOTA: Para los métodos POST, PUT y DELETE siempre se devuelve los datos del elemento al que se le está aplicando la operación, es decir, si ejecutó DELETE se devuelve los datos del registro eliminado. Los parámetros enviados en las peticiones corresponden a los nombres de los campos en las tablas correspondientes de la base de datos.

HOST = https://ide.c9.io/desarrolladoraplicacion/taxis/public/api/v1

### Iniciar Sesión JWT
    HTTP: POST
    Parametros: email, password
    URL: HOST/authjwt
    Respuesta: token (En caso de ser un Conductor el que inicia sesión, también devuelve token_tracking)

"data": {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjgsImlzcyI6Imh0dHBzOlwvXC9vY3Vwb3RheGkubmV0XC9hcGlcL3B1YmxpY1wvYXBpXC92MVwvYXV0aGp3dCIsImlhdCI6MTQ3MTU1NzAzOSwiZXhwIjoxNTAzMDkzMDM5LCJuYmYiOjE0NzE1NTcwMzksImp0aSI6ImUzOThhZmUyYzE5YzA2NjZjOWM3Zjg2YjY1NzZiYzgzIn0.kybxYZ5IsjR6lDa09JTKuTNCMJwFpD__PPyagaKGt8E",
    "token_tracking": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb25kdWN0b3JfaWQiOjgsInRpcG8iOiIzIiwiaWF0IjoxNDcxNTU3MDM5LCJleHAiOjE1MDMwOTMwMzl9.SE-w5o8GkFiQaCWX8rE0kkmR_-R3k1jQPeDhXUxIgy4",
    "usuario_id": 8
  },
    
### Refresh Token
    HTTP: DELETE
    Parametros:
    URL: HOST/refreshjwt
    Respuesta: token
    Header: Se tiene que enviar en el header el token actual para obtener uno nuevo
        Authorization: Bearer [token]

#### A partir de este servicio será obligatorio enviar la cabecera Authorization con el valor del token (Inciando con la palabra clave Bearer)

### Obtener información del usuario a partir de su Token
    HTTP: GET
    Parametros:
    URL: HOST/userjwt
    Respuesta: Array con los datos del usuario

### Obtener archivo rtf con la información de términos y condiciones de uso
    HTTP: GET
    Parametros:
    URL: HOST/usuarios/terminos
    Respuesta: Archivo rtf


### PASAJERO

### Registrar
    HTTP: POST
    Parametros: Datos del usuario y pasajero (Se envían los datos de las tablas usuarios y pasajeros en el mismo array)
                Si se envía el parámetro foto el Content-Type debe ser multipart/form-data
    URL: HOST/pasajeros/registrar
    Respuesta: Datos del usuario/pasajero registrado

### Actualizar
    HTTP: PUT
    Parametros: Datos a ser actualizados
    URL: HOST/pasajeros/actualizar
    Respuesta: Datos del usuario/pasajero actualizado

### Recuperar contraseña
    HTTP: POST
    Parametros: email
    URL: HOST/password/email
    Respuesta: Envía un correo al correo proporcionado con el link para cambiar su contraseña

### Inicio sesión FB
    HTTP: POST
    Parametros: facebook_id, email, nombre
    URL: HOST/pasajeros/authfb
    Respuesta: token

### Info Solicitud de taxi
    HTTP: GET
    Parametros:
    URL: HOST/pasajeros/solicitudes/[id]
    Respuesta: Array con información de la solicitud

### Solicitar taxi
    HTTP: POST
    Parametros: latitud, longitud, detalles (No es necesario enviarle ID del pasajero, se obtiene a partir de su token)
    URL: HOST/pasajeros/solicitudes/
    Respuesta: Array con información de la solicitud

### Cancelar taxi
    HTTP: DELETE
    Parametros:
    URL: HOST/pasajeros/solicitudes/[id]
    Respuesta: Datos de la solicitud (No se elimina la solicitud, solo se establece el estado a cancelado)

### Calificar taxi
    HTTP: PUT
    Parametros: calificacion
    URL: HOST/pasajeros/solicitudes/[id]
    Respuesta: Datos de la solicitud

### Subir Foto
    HTTP: POST (Content-Type: multipart/form-data)
    Parametros: foto
    URL: HOST/pasajeros/uploadfoto
    Respuesta: Confirmación si se subió la foto

### Obtener Foto
    HTTP: GET
    Parametros: id
    URL: HOST/usuarios/foto/[id]
    Respuesta: Archivo de imagen

### Obtener histórico de Solicitudes
    HTTP: GET
    Parametros: (Opcionales), fecha_abordaje: YYYY-MM-DD, estado, page. Si no se envía ningun parámetro devuelve todas las solicitudes del pasajero, si se envía fecha devuelve todas aquellas solicitudes atendidas de la fecha establecida en adelante (fecha>=)
    URL: HOST/pasajeros/solicitudes
    Respuesta: Listado de solicitudes


### CONDUCTOR

### Establecer modo en servicio
    HTTP: PUT
    Parametros: en_servicio=1
    URL: HOST/conductores/modoservicio
    Respuesta: en_servicio

### Atender una solicitud de taxi del cliente
    HTTP: PUT
    Parametros: tiempo_llegada (El ID del conductor se obtiene a partir del token)
    URL: HOST/conductores/solicitudes/[id]
    Respuesta: Datos de la solicitud

### Descargar info de Solicitud de taxi
    HTTP: GET
    Parametros:
    URL: HOST/conductores/solicitudes/[id]
    Respuesta: Datos de la solicitud

### Declarar Abordaje de cliente
    HTTP: PUT
    Parametros:
    URL: HOST/conductores/solicitudes/[id]/abordar
    Respuesta: Datos de la solicitud

### Declarar Fin de servicio (arribo a destino cliente)
    HTTP: PUT
    Parametros:
    URL: HOST/conductores/solicitudes/[id]/finalizar
    Respuesta: Datos de la solicitud

### Cancelar servicio
    HTTP: DELETE
    Parametros:
    URL: HOST/conductores/solicitudes/[id]
    Respuesta:

### Obtener histórico de Solicitudes atendidas
    HTTP: GET
    Parametros: (Opcionales), fecha_abordaje: YYYY-MM-DD, estado, page. Si no se envía ningun parámetro devuelve todas las solicitudes que ha atentido el conductor, si se envía fecha devuelve todas aquellas solicitudes atendidas de la fecha establecida en adelante (fecha>=)
    URL: HOST/conductores/solicitudes
    Respuesta: Listado de solicitudes
