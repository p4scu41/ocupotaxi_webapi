<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearConductoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conductores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('taxi_id')->unsigned();
            $table->char('turno',1); // M -> matutino V -> vespertino A -> Ambos
            
            
            $table->string('nombre');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            $table->char('sexo',1); // M -> Masculino F -> Femenino
            $table->date('fecha_nacimiento');
            $table->string('curp',18);
            $table->string('ine',18);
            
            $table->string('licencia_conducir',10);
            $table->date('vigencia_licencia_conducir');
            
            
            $table->string('direccion');
            $table->string('colonia');
            $table->string('numero');
            $table->integer('municipio_id')->unsigned();            
            $table->string('telefono',15); 
            $table->string('email')->unique();
            
            $table->string('numero_certificado_aptitud');
            
            $table->string('usuario')->unique();
            $table->string('password');
            
            $table->string('avatar');
            
            
            $table->timestamps();            
            
            $table->foreign('taxi_id')
                  ->references('id')->on('taxis')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
                  
            $table->foreign('municipio_id')
                  ->references('id')->on('municipios')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('conductores');
    }
}
