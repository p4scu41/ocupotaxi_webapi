<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearPasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasajeros', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            $table->char('sexo',1); // M -> Masculino F -> Femenino
            $table->string('telefono_celular',15)->nullable(); 
            $table->string('email')->unique();
            
            $table->string('avatar')->nullable();
            
            $table->string('usuario')->unique();
            $table->string('password');
            
            $table->timestamps();          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pasajeros');
    }
}
