<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTaxisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('concesion_id')->unsigned();
            $table->integer('modalidad_id')->unsigned();
            $table->string('tipo');
            $table->string('marca');
            $table->string('modelo',4);            
            $table->string('numero_motor',25);
            $table->string('numero_serie',25);
            
            
            $table->string('numero_economico',4);
            $table->string('tarjeta_circulacion',10);
            $table->string('placas',7);
            
            $table->integer('capacidad');
            $table->string('factura');
            
            $table->integer('aseguradora_id')->unsigned();
            $table->string('poliza_seguro');
            $table->date('vigencia_poliza');
            
            $table->timestamps();            
            
            $table->foreign('concesion_id')
                  ->references('id')->on('concesiones')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
                  
            $table->foreign('modalidad_id')
                  ->references('id')->on('modalidades')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
            
            $table->foreign('aseguradora_id')
                  ->references('id')->on('aseguradoras')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taxis');
    }
}
