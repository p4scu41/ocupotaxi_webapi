<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearConcesionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concesiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_concesion',50);
            $table->string('nombre_concesionado');
            $table->string('nombre_representante_legal');
            $table->string('domicilio');
            $table->integer('municipio_id')->unsigned();
            $table->string('localidad');    
            $table->string('telefono',15);        
            $table->date('fecha_concesion');
            $table->string('acta_constitutiva');
            $table->string('periodico_oficial');
            $table->date('fecha_publicacion');
            $table->string('ine',18);
            
            
            $table->string('telefono_casa',15);
            $table->string('telefono_celular',15);
            
            $table->timestamps();            
            
            $table->foreign('municipio_id')
                  ->references('id')->on('municipios')
                  ->onUpdate('cascade')
                  ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('concesiones');
    }
}
