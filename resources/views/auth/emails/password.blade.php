<h3>Actualización de contraseña.</h3>
<p>De click en el siguiente enlace para actualizar su contraseña: <p>
<p><strong><a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a></strong></p>
