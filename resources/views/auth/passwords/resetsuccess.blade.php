@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Actualización de Contraseña</div>

                <div class="panel-body">
                    <h4 class="text-center">Actualización exitosa de su contraseña</h4>
                    <h4 class="text-center">Ahora utilice su nueva contraseña para iniciar sesión.</h4 class="text-center">
                    <div class="text-center"><i class="fa fa-thumbs-up fa-2x"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
